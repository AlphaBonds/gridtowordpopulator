﻿namespace Instructions
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.instructionView1 = new Instructions.InstructionView();
            this.SuspendLayout();
            // 
            // instructionView1
            // 
            this.instructionView1.AccessibleDescription = "Displays HTML instructions using web view";
            this.instructionView1.AccessibleName = "HTML Instructions Viewer";
            this.instructionView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.instructionView1.Location = new System.Drawing.Point(0, 0);
            this.instructionView1.Name = "instructionView1";
            this.instructionView1.Size = new System.Drawing.Size(800, 450);
            this.instructionView1.TabIndex = 0;
            this.instructionView1.Load += new System.EventHandler(this.instructionView1_Load);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.instructionView1);
            this.Name = "MainForm";
            this.Text = "Instructions";
            this.ResumeLayout(false);

        }

        #endregion

        private InstructionView instructionView1;
    }
}

