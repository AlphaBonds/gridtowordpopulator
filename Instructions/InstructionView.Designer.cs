﻿namespace Instructions
{
    partial class InstructionView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_TableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.m_WebBrowser = new System.Windows.Forms.WebBrowser();
            this.m_Exit = new System.Windows.Forms.Button();
            this.m_TableLayout.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_TableLayout
            // 
            this.m_TableLayout.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.m_TableLayout.ColumnCount = 2;
            this.m_TableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.m_TableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.m_TableLayout.Controls.Add(this.m_WebBrowser, 0, 1);
            this.m_TableLayout.Controls.Add(this.m_Exit, 1, 0);
            this.m_TableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_TableLayout.Location = new System.Drawing.Point(0, 0);
            this.m_TableLayout.Name = "m_TableLayout";
            this.m_TableLayout.RowCount = 2;
            this.m_TableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.m_TableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.m_TableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.m_TableLayout.Size = new System.Drawing.Size(720, 530);
            this.m_TableLayout.TabIndex = 0;
            // 
            // m_WebBrowser
            // 
            this.m_TableLayout.SetColumnSpan(this.m_WebBrowser, 2);
            this.m_WebBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_WebBrowser.Location = new System.Drawing.Point(3, 34);
            this.m_WebBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.m_WebBrowser.Name = "m_WebBrowser";
            this.m_TableLayout.SetRowSpan(this.m_WebBrowser, 2);
            this.m_WebBrowser.Size = new System.Drawing.Size(714, 493);
            this.m_WebBrowser.TabIndex = 1;
            this.m_WebBrowser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.m_WebBrowser_DocumentCompleted);
            // 
            // m_Exit
            // 
            this.m_Exit.BackColor = System.Drawing.Color.Transparent;
            this.m_Exit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_Exit.Location = new System.Drawing.Point(684, 3);
            this.m_Exit.Name = "m_Exit";
            this.m_Exit.Size = new System.Drawing.Size(33, 25);
            this.m_Exit.TabIndex = 0;
            this.m_Exit.Text = "X";
            this.m_Exit.UseVisualStyleBackColor = false;
            this.m_Exit.Click += new System.EventHandler(this.m_Exit_Click);
            // 
            // InstructionView
            // 
            this.AccessibleDescription = "Displays HTML instructions using web view";
            this.AccessibleName = "HTML Instructions Viewer";
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.m_TableLayout);
            this.Name = "InstructionView";
            this.Size = new System.Drawing.Size(720, 530);
            this.m_TableLayout.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel m_TableLayout;
        private System.Windows.Forms.Button m_Exit;
        private System.Windows.Forms.WebBrowser m_WebBrowser;
    }
}
