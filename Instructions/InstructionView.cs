﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Reflection;
using System.Resources;
using Instructions.Properties;

namespace Instructions
{
    public partial class InstructionView : UserControl
    {
        public delegate void CloseDelegate();
        public CloseDelegate ResumeForm;

        public InstructionView()
        {
            InitializeComponent();
            m_WebBrowser.ObjectForScripting = true; 
            LoadHtml();
        }

        private void m_WebBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }

        public static Stream GenerateStreamFromString(string s)
        {
            byte[] byteArray = Encoding.ASCII.GetBytes(s);
            MemoryStream stream = new MemoryStream(byteArray);

            return stream;
        }

        private void LoadHtml()
        {
            string html = Sites.ResourceManager.GetString("InstructionFile");
            var source = GenerateStreamFromString(html);

            m_WebBrowser.DocumentStream = source;
        }

        private void m_Exit_Click(object sender, EventArgs e)
        {
            //this.Visible = false;
            //this.Hide();
            ResumeForm();
            try
            {
                this.Dispose();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
    }
}
