﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace XmlDocumentSupport
{
    public static class XmlMethods
    {

        /// <summary>
        /// Check whether the current node's children contain a specific node
        /// </summary>
        /// <param name="nodeName"></param>
        /// <param name="rootNode"></param>
        /// <param name="nodeList"></param>
        /// <returns></returns>
        private static bool TopLevelContainsNode(string nodeName, XmlNode rootNode, ref List<XmlNode> nodeList)
        {
            List<XmlNode> xmlNodeTemp = new List<XmlNode>();

            foreach (XmlNode xmlNode in rootNode.ChildNodes)
            {
                if (xmlNode.Name == nodeName)
                {
                    return true;
                }

                xmlNodeTemp.Add(xmlNode);             
            }

            nodeList = xmlNodeTemp;

            return false;
        }

        /// <summary>
        /// Checks to see whether Xml document contains a particular node
        /// </summary>
        /// <param name="nodeName">Name of node to search for</param>
        /// <param name="xmlDocument">Document with XML already loaded</param>
        /// <param name="level">Level(s) to search</param>
        /// <returns></returns>
        public static bool ContainsNode(string nodeName, XmlDocument xmlDocument, int level = 0)
        {
            XmlNode rootNode = xmlDocument.DocumentElement;

            if (rootNode.Name == nodeName)
            {
                return true;
            }

            return ContainsNode(nodeName, rootNode, level); 
        }

        /// <summary>
        /// Determines if the direct children of a node contain a node
        /// </summary>
        /// <param name="nodeName"></param>
        /// <param name="rootNode"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        public static bool ContainsNode(string nodeName, XmlNode rootNode, int level = 0)
        {
            List<XmlNode> xmlNodeTemp = new List<XmlNode>();

            if (level == 0)
            {
                return TopLevelContainsNode(nodeName, rootNode, ref xmlNodeTemp);
            }

            while (level > 0)
            {
                foreach (XmlNode xmlNode in xmlNodeTemp)
                {
                    if (TopLevelContainsNode(nodeName, xmlNode, ref xmlNodeTemp))
                    {
                        return true;
                    }
                }

                level--;
            }

            return false;
        }


        /// <summary>
        /// Gets the current node's children specified node
        /// </summary>
        /// <param name="nodeName"></param>
        /// <param name="rootNode"></param>
        /// <param name="nodeList"></param>
        /// <returns></returns>
        private static XmlNode GetTopLevelChildNode(string nodeName, XmlNode rootNode, ref List<XmlNode> nodeList)
        {
            List<XmlNode> xmlNodeTemp = new List<XmlNode>();

            foreach (XmlNode xmlNode in rootNode.ChildNodes)
            {
                if (xmlNode.Name == nodeName)
                {
                    return xmlNode;
                }

                xmlNodeTemp.Add(xmlNode);
            }

            nodeList = xmlNodeTemp;

            return null;
        }

        public static XmlNode GetXmlNodeChild(string nodeName, XmlNode rootNode, int level = 0)
        {
            List<XmlNode> xmlNodeTemp = new List<XmlNode>();

            if (level == 0)
            {
                return GetTopLevelChildNode(nodeName, rootNode, ref xmlNodeTemp);
            }

            while (level > 0)
            {
                foreach (XmlNode xmlNode in xmlNodeTemp)
                {
                    XmlNode result = GetTopLevelChildNode(nodeName, xmlNode, ref xmlNodeTemp);
                    if (result != null)
                    {
                        return result;
                    }
                }

                level--;
            }

            return null;
        }


        /// <summary>
        /// Trims all leading and trailing whitespace in the inner text of all nodes in the xml document
        /// </summary>
        /// <param name="xmlDocument"></param>
        public static void TrimAllNodes(ref XmlDocument xmlDocument)
        {
            XmlNode rootNode = xmlDocument.DocumentElement;
            rootNode.InnerText = rootNode.InnerText.Trim();
            TrimAllNodes(rootNode);
        }

        public static void TrimAllNodes(XmlNode rootNode)
        {

            for (int i = 0; i <rootNode.ChildNodes.Count; i++)
            {
                rootNode.ChildNodes[i].InnerText = rootNode.InnerText.Trim();

                if (rootNode.ChildNodes[i].HasChildNodes)
                {
                    //XmlNode tempNode = rootNode.ChildNodes[i];
                    XmlMethods.TrimAllNodes(rootNode.ChildNodes[i]);
                    //rootNode.ChildNodes[i] = tempNode;
                    //rootNode.ReplaceChild(rootNode.ChildNodes[i], tempNode);
                }

            }
        }

        /// <summary>
        /// Clean XML string to remove all carriage returns and indents
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static string CleanXmlString(string xml)
        {
            string replaced_xml = xml.Replace("\n", string.Empty);
            replaced_xml = replaced_xml.Replace("\r", string.Empty);
            replaced_xml = replaced_xml.Replace("\v", string.Empty);
            replaced_xml = replaced_xml.Replace("\a", string.Empty);

            replaced_xml = replaced_xml.Replace("&", "&#38;");

            return replaced_xml;
        }
    }


}
