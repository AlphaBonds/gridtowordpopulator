﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
//using IronXL;
using MethodResult;
using Microsoft.Office.Tools.Excel;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Xml;
using System.Xml.Linq;

namespace ExcelManipulator
{
    public class ExcelDocumentUser
    {
        private string m_FileName = string.Empty;

        public const string SINGLE_CELL = "SingleCell";
        public const string CELL_RANGE = "CellRange";

        public enum ErrorTypes
        {
          
        }

        /// <summary>
        /// Only works with these document types
        /// </summary>
        public static readonly List<string> AllowedExtentions = new List<string>() { ".CSV", ".XLS", ".XLSX" };

        /// <summary>
        /// Dictionary of all worksheets contained in the workbook
        /// </summary>
        //private Dictionary<string, WorkSheet> m_WorkSheets = new Dictionary<string, WorkSheet>();

        /// <summary>
        /// List of all sheets required in the workbook
        /// </summary>
        private List<string> m_RequiredSheets = new List<string>();

        public ExcelDocumentUser()
        {

        }

        public Stream GetStream(WorkbookPart wbp)
        {
            return wbp.GetStream();
        }
        public void GetSheetInfo(string fileName)
        {
            // Open file as read-only.
            using (SpreadsheetDocument mySpreadsheet = SpreadsheetDocument.Open(fileName, false))
            {
                var sheets = mySpreadsheet.WorkbookPart.Workbook.Sheets;

                // For each sheet, display the sheet information.
                foreach (var sheet in sheets)
                {
                    foreach (var attr in sheet.GetAttributes())
                    {
                        Console.WriteLine("{0}: {1}", attr.LocalName, attr.Value);
                    }
                }
            }
        }

        public void Testing(string worksheet, int row, string column)
        {
            m_FileName = @"C:\GIT_ROOT\gridtowordpopulator\TempResources\TEMPLATES - DO NOT DELETE (4)\TEMPLATES - DO NOT DELETE\10. Excel Format\Moyer Realty Excel Template.xlsx";

            using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(m_FileName, false))
            {
                var wb = spreadsheetDocument.WorkbookPart.Workbook;
                var wbs = wb.Sheets;

                string result = string.Empty;

            }

        }

        public string GetWorksheetText(string worksheet)
        {
            string result = string.Empty;

            //testing
            m_FileName = @"C:\GIT_ROOT\gridtowordpopulator\TempResources\TEMPLATES - DO NOT DELETE (4)\TEMPLATES - DO NOT DELETE\10. Excel Format\Moyer Realty Excel Template.xlsx";

            using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(m_FileName, false))
            {
                var wbp = spreadsheetDocument.WorkbookPart;
                var worksheets = wbp.Workbook.Sheets.ChildElements;

                bool found = false;

                DocumentFormat.OpenXml.Spreadsheet.Sheet selectedSheet = null;
                WorksheetPart wsp = null;

                foreach (DocumentFormat.OpenXml.Spreadsheet.Sheet sheet in worksheets)
                {
                    if (worksheet == sheet.Name)
                    {
                        selectedSheet = sheet;
                        wsp = (WorksheetPart)wbp.GetPartById(sheet.Id);
                        found = true;
                        break;
                    }
                }

                if (!found)
                {

                }
                else
                {
                    using (StreamReader stream = new StreamReader(wsp.GetStream()))
                    {
                        result = stream.ReadToEnd();
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Compares two column values. Not necessary. More efficient to count number of columns and iterate over that many times
        /// </summary>
        /// <param name="column1"></param>
        /// <param name="column2"></param>
        /// <returns></returns>
        public bool ColumnLessThanEqualTo(string column1, string column2)
        {
            if (column1.Length < column2.Length)
            {
                return true;
            }
            else if (column1.Length > column2.Length)
            {
                return false;
            }
            else
            {
                for (int i = 0; i <column1.Length; i++)
                {
                    if (column1[i] < column2[i])
                    {
                        return true;
                    }
                    else if (column1[i] > column2[i])
                    {
                        return false;
                    }
                }

                return true;
            }
        }

        public static string RepeatChar(char ch, int len)
        {
            return new string('A', len);
        }

        public static char IncrementChar(char ch)
        {
           return (char)(Convert.ToInt32(ch) + 1);
        }

        public static string IncrementColumn(string column)
        {
            bool found = false;

            int i;
            for (i = column.Length-1; i >= 0; i--)
            {
                if (column[i] < 'Z')
                {
                    found = true;
                    int indx = column.Length - 1 - i;
                    string firstPart =  i > 0 ? column.Substring(0, column.Length - indx - 1) : string.Empty;
                    char ch = IncrementChar(column[i]);
                    string lastPart = RepeatChar('A', column.Length - i - 1);

                    return firstPart + ch + lastPart;
                }
            }

            if (!found)
            {
                return RepeatChar('A', column.Length + 1);
            }

            return string.Empty;
        }

        public string GetCellXml(Cell cell, SharedStringTablePart stringTable)
        {
            string result = string.Empty;

            // If the cell does not exist, return an empty string.
            if (cell != null)
            {
                if (cell.DataType == "s")
                {
                    if (cell.DataType.Value == CellValues.SharedString)
                    {

                        int indx = int.Parse(cell.CellValue.InnerText);
                        string actualVal = stringTable.RootElement.ChildElements[indx].InnerText;
                        int startIndx = cell.InnerXml.IndexOf('>');
                        int endIndx = cell.InnerXml.IndexOf('<', startIndx);
                        result = cell.InnerXml.Substring(0, startIndx + 1) + actualVal + cell.InnerXml.Substring(endIndx);

                    }
                    else
                    {
                        return cell.InnerXml;
                    }
                }
                else if (cell.DataType != null)
                {
                    if (cell.StyleIndex != null)
                    {

                        //result = theCell.InnerText;
                        // Code removed here…
                        return cell.InnerXml;
                    }
                }
            }

            return result;
        }

        public string GetMultipleCellValues(string fileName, string sheetName, string startColumn, int startRow, string endColumn, int endRow)
        {
            string result = string.Empty;
            string avoidColumn = IncrementColumn(endColumn);


            using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(fileName, false))
            {
                // Retrieve a reference to the workbook part.
                WorkbookPart wbPart = spreadsheetDocument.WorkbookPart;

                // Find the sheet with the supplied name, and then use that 
                // Sheet object to retrieve a reference to the first worksheet.
                Sheet theSheet = wbPart.Workbook.Descendants<Sheet>().
                  Where(s => s.Name == sheetName).FirstOrDefault();

                // Throw an exception if there is no sheet.
                if (theSheet == null)
                {
                    throw new ArgumentException("sheetName");
                }

                // Retrieve a reference to the worksheet part.
                WorksheetPart wsPart = (WorksheetPart)(wbPart.GetPartById(theSheet.Id));

                var stringTable = wbPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();

                for (int row = startRow; row <= endRow; row++)
                {
                    //create the xml for the row by getting the column outer tags and styling
                    result += "";

                    var cells = wsPart.Worksheet.Descendants<Cell>();

                    for (string col = startColumn; col != avoidColumn; IncrementColumn(col))
                    {
                        string addressName = CreateCellAddress(col, row);

                        // Use its Worksheet property to get a reference to the cell 
                        // whose address matches the address you supplied.
                        Cell theCell = wsPart.Worksheet.Descendants<Cell>().
                            Where(c => c.CellReference == addressName).FirstOrDefault();

                        string cellXml = GetCellXml(theCell, stringTable);

                        //add it to the main string
                        result += cellXml;
                    }

                    //closing tag
                    result += "";
                }
            }

            return result;
        }

        public string GetCellValue(string fileName, string sheetName, string column, int row)
        {
            string addressName = CreateCellAddress(column, row);

            string result = string.Empty;

            // Open the spreadsheet document for read-only access.
            using (SpreadsheetDocument document = SpreadsheetDocument.Open(fileName, false))
            {
                // Retrieve a reference to the workbook part.
                WorkbookPart wbPart = document.WorkbookPart;

                // Find the sheet with the supplied name, and then use that 
                // Sheet object to retrieve a reference to the first worksheet.
                Sheet theSheet = wbPart.Workbook.Descendants<Sheet>().
                  Where(s => s.Name == sheetName).FirstOrDefault();

                // Throw an exception if there is no sheet.
                if (theSheet == null)
                {
                    throw new ArgumentException("sheetName");
                }

                // Retrieve a reference to the worksheet part.
                WorksheetPart wsPart = (WorksheetPart)(wbPart.GetPartById(theSheet.Id));

                var stringTable = wbPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();

                //do we want to replace the outer or the inner xml of the cell?

                // Use its Worksheet property to get a reference to the cell 
                // whose address matches the address you supplied.
                Cell theCell = wsPart.Worksheet.Descendants<Cell>().
                    Where(c => c.CellReference == addressName).FirstOrDefault();

                //var testing = wsPart.Worksheet.Descendants<Cell>();

                string cellXml = GetCellXml(theCell, stringTable);

            }       

            return result;
        }

        private static string CreateCellAddress(string column, int row)
        {
            return (column + row.ToString());
        }

        public string GetMultiCellText(string worksheet, int leftMost, int rightMost, int topRow, int bottomRow)
        {
            string result = string.Empty;

            string xml = GetWorksheetText(worksheet);

            //Now create the xml document and parse it
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(xml);

            //build it


            return result;
        }

        ///// <summary>
        ///// Loads an excel document
        ///// </summary>
        ///// <param name="filePath"></param>
        ///// <returns></returns>
        //public ReturnValue LoadExcelDoc(string filePath)
        //{
        //    ReturnValue ret = new ReturnValue();
        //    ValidateFileName(filePath, ret);

        //    if (!ret.Success)
        //    {
        //        return ret;
        //    }

        //    try
        //    {
        //        //Supported spreadsheet formats for reading include: XLSX, XLS, CSV and TSV
        //        WorkBook workbook = WorkBook.Load(filePath);

        //        foreach (WorkSheet ws in workbook.WorkSheets)
        //        {
        //            m_WorkSheets[ws.Name] = ws;
        //        }

        //        ValidateSheets(ret);
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine(ex.Message);
        //        ret.Success = false;
        //        ret.StatusMessage = string.Format(Strings.FileWouldNotLoad, filePath);
        //        return ret;
        //    }

        //    return ret;
        //}

        /// <summary>
        /// Validates the excel sheets to make sure all sheets are there
        /// </summary>
        /// <param name="ret"></param>
        public void ValidateSheets(ReturnValue ret)
        {
            //store the indices of the missing sheets
            List<string> missingSheets = new List<string>();

            foreach (string s in m_RequiredSheets)
            {
                //if (!m_WorkSheets.ContainsKey(s))
                //{
                //    missingSheets.Add(s);
                //}
            }

            if (missingSheets.Count > 0)
            {
                ret.StatusMessage = string.Format(Strings.MissingWorksheets, string.Join(", ", missingSheets));
                ret.Success = false;
            }
            else
            {
                ret.SetNoIssues();
            }
        }

        /// <summary>
        /// Validates the file name to ensure that the path exists and the extension is valid
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="ret"></param>
        public void ValidateFileName(string filePath, ReturnValue ret)
        {
            if (!File.Exists(filePath))
            {
                ret.Success = false;
                ret.StatusMessage = string.Format(Strings.FileDoesNotExist, filePath);
            }
            else
            {
                if (!AllowedExtentions.Contains(Path.GetExtension(filePath)))
                {
                    ret.Success = false;
                    ret.StatusMessage = string.Format(Strings.InvalidFileExtension, filePath);
                }
                else
                {
                    ret.SetNoIssues();
                }
            }
        }
    }
}
