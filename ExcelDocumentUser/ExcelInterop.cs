﻿using System;
using System.Diagnostics;
using System.IO;
using DocumentFormat.OpenXml.Drawing.Charts;
using Microsoft.Office.Interop.Excel;
using _Excel = Microsoft.Office.Interop.Excel;

namespace ExcelManipulator
{
    public class ExcelInterop : IDisposable
    {
        private string m_FilePath = string.Empty;
        private static _Application m_ExcelApp = new _Excel.Application();
        private Workbook m_Workbook = null;
        private Worksheet m_Worksheet = null;

        public ExcelInterop()
        {
            m_Workbook = null;
            m_Worksheet = null;
        }

        public ExcelInterop(string filePath)
        {
            m_Workbook = null;
            m_Worksheet = null;
            string m_FilePath = filePath;
            m_Workbook = m_ExcelApp.Workbooks.Open(m_FilePath);
        }

        public ExcelInterop(string copyName, string filePath)
        {
            m_Workbook = null;
            m_Worksheet = null;
            File.Copy(filePath, copyName);
            string m_FilePath = copyName;
            m_Workbook = m_ExcelApp.Workbooks.Open(m_FilePath);
        }

        ~ExcelInterop()
        {
            CloseIfNeeded();
            m_Workbook = null;
            m_Worksheet = null;
            //CloseFile();
            CloseApp();
        }

        private static void CloseApp()
        {
            try
            {
                m_ExcelApp.Application.Quit();
            }
            catch
            {

            }
        }

        public void CloseFile()
        {
            m_FilePath = string.Empty;
            object saveFile = false;

            try
            {
                if (m_Workbook != null)
                {
                    m_Workbook.Close(SaveChanges: saveFile);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        public void LoadWorkbook(string filePath, DeletePrevious deletePrevious = null)
        {
            CloseIfNeeded();
            m_Workbook = null;
            m_Worksheet = null;

            m_FilePath = filePath;
            m_Workbook = m_ExcelApp.Workbooks.Open(m_FilePath);

            DeletePreviousFile(deletePrevious);
        }

        public void LoadWorkbook(string copyName, string filePath, DeletePrevious deletePrevious = null)
        {
            CloseIfNeeded();
            m_Workbook = null;
            m_Worksheet = null;

            File.Copy(filePath, copyName);
            string formerPath = m_FilePath;
            m_FilePath = copyName;
            m_Workbook = m_ExcelApp.Workbooks.Open(m_FilePath);

            try
            {
                if (formerPath != string.Empty)
                {
                    DeletePreviousFile(deletePrevious);
                }
            }
            catch
            {

            }
        }

        private void CloseIfNeeded()
        {
            if (m_Workbook != null)
            {
                CloseFile();
                //m_FilePath = string.Empty;
            }
        }

        public void LoadWorksheet(string sheetName)
        {
            m_Worksheet = null;

            int sheetNum = FindWorksheetNum(sheetName);

            if (sheetNum == -1)
            {
                //Throw a custom exception that will be caught at the base, and used to generate error messages
            }

            m_Worksheet = m_Workbook.Worksheets[sheetNum];
        }

        public void DeletePreviousFile(DeletePrevious deletePrevious)
        {
            if (deletePrevious != null)
            {
                if (deletePrevious.Delete && deletePrevious.FileName != string.Empty)
                {
                    File.Delete(deletePrevious.FileName);
                }
            }
        }

        private int FindWorksheetNum(string sheetName)
        {
            //Get worksheet number
            int i = 1;

            foreach (Worksheet ws in m_Workbook.Worksheets)
            {
                string wsName = ws.Name.Trim().ToUpper();
                if (wsName == sheetName)
                {
                    return i;
                }
                else
                {
                    i++;
                }
            }

            return -1;
        }

        //private static string ConvertDateToString(in string dateStr)
        //{
        //    DateTime result = Convert.ToDateTime(dateStr);
        //    result.GetDateTimeFormats()
        //}

        public string ReadCell(object row, object column, bool isDateTime = false)
        {
            try
            {
                if (!isDateTime)
                {
                    if (m_Worksheet.Cells[row][column].Value != null)
                    {
                        return Convert.ToString(m_Worksheet.Cells[row][column].Value2);
                    }
                    else if (m_Worksheet.Cells[row, column].Value != null)
                    {
                        return Convert.ToString(m_Worksheet.Cells[row, column].Value2);
                    }
                }
                else
                {
                    DateTime dt;

                    if (m_Worksheet.Cells[row, column].Text != null)
                    {
                        dt = Convert.ToDateTime(Convert.ToString(m_Worksheet.Cells[row, column].Text));
                    }
                    else if (m_Worksheet.Cells[row][column].Text != null)
                    {
                        dt = Convert.ToDateTime(Convert.ToString(m_Worksheet.Cells[row][column].Text));
                    }
                    else
                    {
                        throw new Exception("Can't find anything here.");
                    }

                    return dt.ToString("MMMM dd, yyyy");
                }
            }
            catch
            {
                //
            }
            

            return string.Empty;
        }

        public Range ReadRange(int startRow, int endRow, int startCol, int endCol)
        {
            return m_Worksheet.Range[m_Worksheet.Cells[startRow, startCol], m_Worksheet.Cells[endRow, endCol]];
        }

        [STAThread]
        public void CopyRangeToClipboard(int startRow, int endRow, int startCol, int endCol)
        {
            var range = ReadRange(startRow, endRow, startCol, endCol);    
            range.Copy();
        }

        public string [,] ReadRangeAsString(int startRow, int endRow, int startCol, int endCol)
        {
            //Range range = (Range) m_Worksheet.Range[m_Worksheet.Cells[startRow, startCol], m_Worksheet.Cells[endRow, endCol]];
            Range range = (Range)ReadRange(startRow, endRow, startCol, endCol);
            object[,] holder = range.Value2;

            int numRows = endRow - startRow;
            int numCols = endCol - startCol;

            string[,] retStr = new string[numRows, numCols];
            
            for (int row = 1;  row <= numRows; row++)
            {
                for (int col = 1; col <= numCols; col++)
                {
                    if (holder[row, col] != null)
                    {
                        retStr[row - 1, col - 1] = holder[row, col].ToString();
                    }
                    else
                    {
                        retStr[row - 1, col - 1] = string.Empty;
                    }
                }
            }

            return retStr;
        }

        public void Dispose()
        {
            
        }
    }

    public class DeletePrevious
    {
        public bool Delete;
        public string FileName;

        public DeletePrevious(bool Delete, string FileName)
        {
            this.Delete = Delete;
            this.FileName = FileName;
        }
    }
}
