﻿using ExcelManipulator;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using FileUsage;
using System.Text.RegularExpressions;
using _Word = Microsoft.Office.Interop.Word;
using System.Xml;
using Microsoft.Office.Interop.Word;
using MacroView.VSTO.Word;
using System.Text;
using Path = System.IO.Path;
using XmlDocumentSupport;
using Microsoft.Office.Core;
using System.Data;
using DocumentFormat.OpenXml.Presentation;
using HeaderFooter = Microsoft.Office.Interop.Word.HeaderFooter;

namespace WordDocManipulator
{
    public class WordInterop : IDisposable
    {
        private static List<string> m_ErrorMessages = new List<string>();

        private string m_FilePath = string.Empty;
        private string m_RootPath = string.Empty;
        private static _Word.Application m_WordApp;
        private static bool m_Initialized = false;
        private _Word.Document m_Document;
        private Dictionary<string, bool> m_AutoTextEntries;
        private bool m_IsTemplateDocument;
        private bool m_UpdaterObj;

        public delegate void AddFileToDictionary(string fileName);
        public delegate void UpdateMethod(int percentComplete);
        public delegate void UpdateMethodText(string text);

        public UpdateMethod UpdateProgressBar = null;
        public UpdateMethodText UpdateText = null;
        public AddFileToDictionary AddFileToDelete = null;

        /// <summary>
        /// Only use one instance of excel interop to avoid constantly opening and closing files
        /// </summary>
        private static ExcelInterop m_ExcelInterop = null;

        /// <summary>
        /// If the name of the file is the same, there is no need to change the workbook
        /// </summary>
        private string m_ExcelFileName = string.Empty;

        /// <summary>
        /// For missing parameter values
        /// </summary>
        private readonly object m_OMissing = System.Reflection.Missing.Value;

        /// <summary>
        /// The real name of the document copied from
        /// </summary>
        private bool IsCopy = false;

        private string ExcelFileToDelete = string.Empty;

        #region Constructors 
        public WordInterop()
        {
            InitWordApp();
            m_FilePath = string.Empty;
            m_UpdaterObj = false;
        }

        public WordInterop(string filePath)
        {
            InitWordApp();
            m_FilePath = filePath;
            CloseIfOpen(m_FilePath);
            m_UpdaterObj = false;

            try
            {
                #region comments
                //TODO: check ahead of time aobut ConfirmConversions - Optional Object. True to display the Convert File dialog box if the file isn't in Microsoft Word format.
                //TODO: Consider AddToRecentFiles - Optional Object. True to add the file name to the list of recently used files at the bottom of the File menu.

                //TODO: What if there is no password? Or if there is? PasswordDocument
                /*
                 * WritePasswordDocument
                Object
                Optional Object. The password for saving changes to the document.

                WritePasswordTemplate
                Object
                Optional Object. The password for saving changes to the template.
                */

                //TODO: Word Template - in the future, should we use a word template maybe instead of word document?


                //TODO: Revert 
                //Optional Object.Controls what happens if FileName is the name of an open document. 
                //True to discard any unsaved changes to the open document and reopen the file. False to activate the open document

                /*
                 * Format
                Object
                Optional Object. The file converter to be used to open the document. 
                Can be a WdOpenFormat constant.To specify an external file format, 
                apply the OpenFormat property to a FileConverter object to determine the value to use with this argument.
                 */

                /*
                 * 
                 * Visible
                Object
                Optional Object. True if the document is opened in a visible window. The default value is True.
                 */

                /*/
                 * OpenAndRepair
                Object
                Optional Object. True to repair the document to prevent document corruption.
                */

                //                XMLTransform
                //Object
                //Optional Object. Specifies a transform to use.
                #endregion

                LoadWordDocument(m_FilePath);
                //object m_OMissing = System.Reflection.Missing.Value;

                //m_WordApp.Documents.Open(FileName: m_FilePath, ConfirmConversions: true, ReadOnly: false, AddToRecentFiles: false, m_OMissing, m_OMissing,
                //    Revert: true, m_OMissing, m_OMissing, m_OMissing, m_OMissing, Visible: true, OpenAndRepair: true, m_OMissing, m_OMissing, m_OMissing);
                //m_Document = m_WordApp.ActiveDocument;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                List<Process> locks = FileManagement.GetProcessesLockingFile(m_FilePath);
            }
        }

        public WordInterop(string copyName, string filePath)
        {
            File.Copy(filePath, copyName);
            InitWordApp();
            m_FilePath = copyName;
            CloseIfOpen(m_FilePath);

            try
            {
                LoadWordDocument(m_FilePath);

                IsCopy = true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                List<Process> locks = FileManagement.GetProcessesLockingFile(m_FilePath);
            }
        }
        #endregion

        #region Destructor

        ~WordInterop()
        {
            if (m_ExcelFileName != string.Empty)
            {
                try
                {
                    DeletePrevious deletePrevious = new DeletePrevious(true, m_ExcelFileName);

                    //TODO: What is this?
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }

            if (IsCopy)
            {
                
            }

            CloseAll();
        }
        #endregion

        public void UnitializeApp()
        {
            m_Initialized = false;
            m_ErrorMessages = new List<string>();
            m_FilePath = string.Empty;
            m_RootPath = string.Empty;
            m_WordApp = null;
        }

        public UpdateMethod Updater 
        {
            get
            {
                return UpdateProgressBar;
            }
            set
            {
                UpdateProgressBar = value;
                m_UpdaterObj = true;
            }

        }

        #region FileManagement
        public string RootPath
        {
            get
            {
                return m_RootPath;
            }
            set 
            { 
                m_RootPath = value;          
            }
        }

        public bool IsTemplateDocument
        {
            get
            {
                return m_IsTemplateDocument;
            }
            set
            {
                m_IsTemplateDocument = value;
            }
        }

        public void InitWordApp()
        {
            m_AutoTextEntries = new Dictionary<string, bool>();
            RootPath = string.Empty;

            if (!m_Initialized)
            {
                m_WordApp = new _Word.Application
                {
                    Visible = false,
                    WindowState = _Word.WdWindowState.wdWindowStateNormal
                };

                m_ExcelInterop = new ExcelInterop();
            }

            m_Initialized = true;
        }

        public string GetFileName()
        {
            return Document.FullName;
        }

        /// <summary>
        /// Loads Word Document from copy of file at filepath
        /// </summary>
        /// <param name="copyName"></param>
        /// <param name="filePath"></param>
        public void LoadWordDocument(string copyName, string filePath)
        {
            // hmm... 
            File.Copy(filePath, copyName);
            m_FilePath = copyName;
            CloseIfOpen(m_FilePath);

            try
            {
                LoadWordDocument(m_FilePath);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                List<Process> locks = FileManagement.GetProcessesLockingFile(m_FilePath);
            }
        }
        
        /// <summary>
        /// Loads Word document from filepath
        /// </summary>
        /// <param name="filePath"></param>
        public void LoadWordDocument(string filePath)
        { 
            m_FilePath = filePath;
            CloseIfOpen(m_FilePath);

            object confirmConversions = true;
            //object confirmConversions = false;
            object readOnly = false;
            object addToRecentFiles = false;
            //object revert = true;
            object revert = false;
            object openAndRepair = false;
            object visible = true;

            try
            {
                m_WordApp.Documents.Open(m_FilePath, m_OMissing, m_OMissing, m_OMissing, m_OMissing, m_OMissing,
                    Revert: revert, m_OMissing, m_OMissing, m_OMissing, m_OMissing, Visible: visible, OpenAndRepair: openAndRepair, m_OMissing, m_OMissing, m_OMissing);

                //m_WordApp.Documents.Open(m_FilePath);
                m_Document = m_WordApp.ActiveDocument;
                m_Document.Activate();
                //m_WordApp.Selection.ClearFormatting();
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                Debug.WriteLine(ex.Message);
                //The RPC server is unavailable. (Exception from HRESULT: 0x800706BA)  - if I recall correctly, this was caused by trying to use the Application when it was already deallocated from memory
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            finally
            {

            }
        }

        public void CloseWordDocument()
        {
            //m_WordApp.Documents.Close();
            try
            {
                m_Document.Close();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        public void SaveWordDocument()
        {
            object noPrompt = false;
            object originalFormat = true;

            //use the root path and file path
            //m_WordApp.Documents.Save(NoPrompt: noPrompt, OriginalFormat: originalFormat);
            //m_Document.Save();

            object filePath = m_FilePath;
            object fileFormat = m_OMissing;//WdSaveFormat.wdFormatDocument;
            object lockContents = false;


            try
            {
                m_WordApp.ActiveDocument.SaveAs2(m_OMissing, fileFormat, lockContents, m_OMissing, m_OMissing, m_OMissing,
                    m_OMissing, m_OMissing, m_OMissing, m_OMissing, m_OMissing, m_OMissing, m_OMissing, m_OMissing, m_OMissing, m_OMissing, m_OMissing);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            finally
            {

            }
        }

        public void ShowDialog(bool show)
        {
            if (show)
            {
                m_WordApp.Visible = true;
            }
            else
            {
                m_WordApp.Visible = false;
            }
        }

        public void SaveAsWordDocument(bool showDialog = false)
        {
            try
            {
                object lockContents = false;
                object addToRecent = true;
                m_WordApp.Options.SaveNormalPrompt = true; 
                m_WordApp.Options.ShowMarkupOpenSave = true;
                //m_WordApp.ActiveDocument.CommandBars("Menu Bar").Controls("&File").Controls("Save &As...").enabled = false;
                m_WordApp.DisplayAlerts = WdAlertLevel.wdAlertsMessageBox;
                //m_WordApp.Dialogs.Application.
            
                try
                {
                    if (showDialog)
                    {
                        //m_WordApp.Visible = true; 
                        var wordDialog = m_WordApp.Dialogs[WdWordDialog.wdDialogFileSaveAs]; //.ThisApplication.Dialogs[Word.WdWordDialog.wdDialogFileSaveAs];
                        //wordDialog.Show();
                        wordDialog.Show();
                    }
                    else
                    {
                        m_WordApp.ActiveDocument.SaveAs2(m_FilePath, m_OMissing, lockContents, m_OMissing, addToRecent, m_OMissing, m_OMissing,
                            m_OMissing, m_OMissing, m_OMissing, m_OMissing, m_OMissing, m_OMissing, m_OMissing, m_OMissing, m_OMissing, m_OMissing);
                    }
                }
                catch (Exception ex)
                {
                    Debug.Write(ex.Message);
                }
            }
            catch
            {

            }

            try
            {
                m_WordApp.Options.SaveNormalPrompt = false;
                m_WordApp.DisplayAlerts = WdAlertLevel.wdAlertsNone;
            }
            catch
            {

            }
        }

        public void CloseIfOpen(string path)
        {
            try
            {
                FileStream fs = File.Open(path, FileMode.Open);
                fs.Close();
            }
            catch (FileNotFoundException ex)
            {
                Debug.WriteLine("File not found : " + ex.Message);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Looks like it's currently being edited : " + ex.Message);
            }
        }
        #endregion

        #region Accessors
        public string FilePath
        {
            get
            {
                return m_FilePath;
            }
        }

        public _Word.Document Document
        {
            get
            {
                return m_Document;
            }
        }

        public _Word.Range Content
        {
            get
            {
                //Debug.WriteLine(Document.Content.XML);

                //foreach (var sentence in Document.Content.Sentences)
                //{
                //    Debug.WriteLine(sentence);
                //    Debug.WriteLine(sentence.GetType());
                //}


                return m_Document.Content;              
            }
        }

        public bool IsOpen
        {
            get
            {
                if (m_Document.IsClosed())
                {
                    return false;
                }

                return true;
            }
        }
        #endregion

        public void LogError(string message)
        {
            string error = $"FilePath : {m_FilePath}; Root Path: {m_RootPath}; Message : {message}";
            m_ErrorMessages.Add(error);
        }

        #region Deprecated 
        public void AddItAllUnfinished(string text, ref List<string> xmlStrings, ref bool unfinished, int startIndex)
        {
            if (unfinished)
            {
                xmlStrings[xmlStrings.Count - 1] += text.Substring(startIndex);
            }
            else
            {
                if (!string.IsNullOrEmpty(text.Substring(startIndex)))
                {
                    xmlStrings.Add(text.Substring(startIndex));
                }
            }

            unfinished = true;
        }

        public void FinishCloseTag(string text, ref int openCount, ref int closeCount, ref List<string> xmlStrings, ref bool unfinished, int startIndex, int endIndex)
        {
            openCount = closeCount = 0;
            int len = endIndex - startIndex;

            if (unfinished)
            {
                xmlStrings[xmlStrings.Count - 1] += text.Substring(startIndex, len);
            }
            else
            {
                xmlStrings.Add(text.Substring(startIndex, len));
            }

            unfinished = false;
        }

        public bool HandleUnopened(string text, ref int openCount, ref List<string> xmlStrings, ref int currIndex, ref int startIndex)
        {
            //m_Selection = new _Word.Selection();
            Match match = Regex.Match(text.Substring(currIndex), TagConstants.ReplaceableOpenTagRegex);

            if (match.Success)
            {
                startIndex = match.Index;
                currIndex = startIndex + match.Length + 1;
                openCount += 1;
            }
            else
            {
                return false;
            }

            return true;
        }

        public bool HandleBothMatch(string text, ref int openCount, ref int closeCount, ref List<string> xmlStrings, ref int currIndex, ref int startIndex, ref bool unfinished, Match openMatch, Match closeMatch)
        {
            if (openMatch.Index < closeMatch.Index)
            {
                openCount += 1;
                currIndex = openMatch.Index;

                //check for other open tags

                while (currIndex < closeMatch.Index)
                {
                    openMatch = Regex.Match(text.Substring(currIndex), TagConstants.ReplaceableOpenTagRegex);

                    if (openMatch.Success)
                    {
                        currIndex = openMatch.Index;
                        openCount += 1;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            else
            {
                currIndex = closeMatch.Index + closeMatch.Index + 1;

                do
                {
                    closeCount += 1;

                    if (closeCount == openCount)
                    {
                        FinishCloseTag(text, ref openCount, ref closeCount, ref xmlStrings, ref unfinished, startIndex, closeMatch.Index + closeMatch.Length);
                    }

                    closeMatch = Regex.Match(text.Substring(currIndex), TagConstants.ReplaceableCloseTagRegex);

                    if (closeMatch.Success)
                    {
                        currIndex = closeMatch.Index + closeMatch.Index + 1;
                    }

                } while (closeMatch.Success);
            }

            return true;
        }

        public void FindReplaceableTags(string text, ref int openCount, ref int closeCount, ref List<string> xmlStrings, ref bool unfinished)
        {
            int startIndex = 0;
            int currIndex = 0;
            string currStr = string.Empty;

            while (startIndex < text.Length)
            {
                if (openCount <= closeCount)
                {
                   if (!HandleUnopened(text, ref openCount, ref xmlStrings, ref currIndex, ref startIndex))
                    {
                        return;
                    }
                }

                // here we add them 
                Match openMatch = Regex.Match(text.Substring(currIndex), TagConstants.ReplaceableOpenTagRegex);
                Match closeMatch = Regex.Match(text.Substring(currIndex), TagConstants.ReplaceableCloseTagRegex);

                if (openMatch.Success && closeMatch.Success)
                {
                    HandleBothMatch(text, ref openCount, ref closeCount, ref xmlStrings, ref currIndex, ref startIndex, ref unfinished, openMatch, closeMatch);
                }
                else if (openMatch.Success)
                {
                    // get the number of open tags and update
                    openCount += 1;

                    MatchCollection collection = Regex.Matches(text.Substring(openMatch.Index + openMatch.Length + 1), TagConstants.ReplaceableOpenTagRegex);
                    openCount += collection.Count;

                    AddItAllUnfinished(text, ref xmlStrings, ref unfinished, startIndex);

                    return;
                }
                else if (closeMatch.Success)
                {
                    //get the remaining number of close tags and update
                    do
                    {
                        closeCount += 1;

                        if (closeCount == openCount)
                        {
                            FinishCloseTag(text, ref openCount, ref closeCount, ref xmlStrings, ref unfinished, startIndex, closeMatch.Index + closeMatch.Length);
                        }

                    } while (openCount < closeCount);

                    return;
                }
                else
                {
                    AddItAllUnfinished(text, ref xmlStrings, ref unfinished, startIndex);
                    return;
                }
            }         
        }
        #endregion

        #region Replacement

        public void FindAndReplaceRange(_Word.Range myRange, string findText, _Word.Range replacementText)
        {
            object matchCase = false;
            object matchWholeWord = true;
            object matchWildCards = false;
            object matchSoundsLike = false;
            object matchAllWordForms = false;
            object forward = true;
            object format = true;
            object matchKashida = false;
            object matchDiacritics = false;
            object matchAlefHamza = false;
            object matchControl = false;
            object read_only = false;
            object visible = true;
            object replace = 2;
            object wrap = 1;

            object replaceable = myRange;

            _Word.Find findObject = m_WordApp.Selection.Find;
            findObject.ClearFormatting();
            findObject.Replacement.ClearFormatting();
            findObject.Text = findText;
            findObject.Replacement.Text = replacementText.Text;
            //findObject.Replacement.ParagraphFormat = replacementText.ParagraphFormat;

            if (m_WordApp.Application.Options.Overtype)
            {
                m_WordApp.Application.Options.Overtype = false;
            }

            try
            {

            }
            catch
            {

            }
            finally
            {

            }

        }

        public void FindAndReplaceText(string findText, string replacementText, Range range)
        {
            //it looks like this is called but no references?
            try
            {
                range.Replace(findText, replacementText);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Replace only the text, while preserving formatting
        /// </summary>
        /// <param name="myRange"></param>
        /// <param name="findText"></param>
        /// <param name="replacementText"></param>
        /// <param name="replaceType"></param>
        public void FindAndReplaceText(_Word.Range myRange, string findText, string replacementText)
        {
            //PROBLEM1

            //myRange.Select();
            //_Word.Find findObject = m_WordApp.Selection.Find;

            //_Word.Find findObject = myRange.Find;

            //findObject.ClearFormatting(); //might be a problem
            //findObject.Replacement.ClearFormatting(); //or this
            //findObject.Text = findText;
            //findObject.Replacement.Text = replacementText; // might be a problem

            //clear formatting for both
            //findObject.ClearFormatting();
            //findObject.Replacement.ClearFormatting();

            object oMissing = m_OMissing;
            //object missing = oMissing;

            //object textToFind = findText.ToString();
            //object matchWholeWord = true;
            //object format = false;
            //object replaceWith = replacementText.ToString();
            object replace = WdReplace.wdReplaceOne;
            //object replaceAll = WdReplace.wdReplaceAll;

            //findObject.Replacement.Text = "Found";

            //findObject.Text = myRange.Text;
            //findObject.Replacement.Text = replacementText.ToString();

            try
            {
                //Instead of replacing using Find.Execute():
                //find a text, get its position, insert new text.
                //That would not limit you in a length of the new string.

                bool result = true;
                //bool result = myRange.Find.Execute(findText);
                myRange.Text = replacementText;


                //this might not actually be the problem
                //we should probably consider stripping the front and end of the string
                //it is possible that it is stripping out whitespace

                //it is also possible that we might be stripping something elsewhere

                //we probably want to avoid copy paste

                //it seems to be specific to tables
                // HERE IS MY PROBLEM... MMK

                //Here is where i was last
                //bool result = findObject.Execute(FindText: ref textToFind, MatchCase: ref oMissing, MatchWholeWord: ref matchWholeWord,
                //    MatchWildcards: ref oMissing, MatchSoundsLike: ref oMissing, MatchAllWordForms: ref oMissing,
                //    Forward: ref oMissing, Wrap: ref oMissing, Format: ref format, ReplaceWith: ref replaceWith,
                //    Replace: ref replace, MatchKashida: ref oMissing, MatchDiacritics: ref oMissing,
                //    MatchAlefHamza: ref oMissing, MatchControl: ref oMissing);

                //bool result = findObject.Execute(ref missing, ref missing, ref missing, ref missing, ref missing,
                //    ref missing, ref missing, ref missing, ref missing, ref missing,
                //    ref replaceAll, ref missing, ref missing, ref missing, ref missing);

                if (result)
                {

                }
                else
                {

                }
            }
            catch
            {

            }
            finally
            {

            }
        }
        #endregion

        public _Word.Range GetRelevantRange(_Word.Range myRange, string findText)
        {
            object matchCase = true;
            object matchWholeWord = true;
            object replace = WdReplace.wdReplaceNone;
            myRange.Find.Execute(findText, matchCase, matchWholeWord, m_OMissing, m_OMissing, m_OMissing, m_OMissing,
                m_OMissing, m_OMissing, m_OMissing, replace, m_OMissing, m_OMissing, m_OMissing, m_OMissing);

            int start = myRange.Start;
            int end = myRange.End;
            
            return m_Document.Range(start, end);
        }

        private void SetRangeStyle(string text, _Word.Range originalRange, _Word.Range replacementRange)
        {
            _Word.Range theRange = GetRelevantRange(originalRange, text);
            theRange.Find(text);
            theRange.Font = replacementRange.Font;
        }

        /// <summary>
        /// Find and replace text
        /// </summary>
        /// <param name="myRange"></param>
        /// <param name="findText"></param>
        /// <param name="replacementText"></param>
        public void FindAndInsert(_Word.Range myRange, string findText, _Word.Range replacementText)
        {
            //_Word.Range theRange = GetRelevantRange(myRange, findText);
            FindAndReplaceText(myRange, findText, replacementText.Text);
            SetRangeStyle(replacementText.Text, myRange, replacementText);
        }

        public void CopyPasteWithFormatting(string findText, _Word.Document document, Range shapeTextRange = null)
        {
            WdRecoveryType wdRecoveryType = WdRecoveryType.wdFormatOriginalFormatting;

            //I need to select the other document with the replacement text, copy it 
            document.Activate();
            document.Content.Copy();

            if (shapeTextRange == null)
            {
                m_Document.Activate();
                m_WordApp.Selection.Find.ClearFormatting();
                _Word.Range replaceRange = m_Document.Content.Find(findText);

                //then, I just paste it where the replacement text is
                replaceRange.PasteAndFormat(wdRecoveryType);
            }
        }

        private void PasteSpecial(string findText)
        { 
            object iconIndex = false;
            object link = false;
            object placement = WdOLEPlacement.wdInLine;
            object displayAsIcon = false;
            object dataType = WdPasteDataType.wdPasteRTF;
            object iconFileName = m_OMissing;
            object iconLabel = m_OMissing;


            m_Document.Activate();
            m_WordApp.Selection.Find.ClearFormatting();

            try
            {
                ////it can only handle up to 255 characters
                if (findText.Length > 255)
                {
                    int charLength = 250;

                    int numSegments = (int)Math.Ceiling((float)findText.Length / (float)charLength);
                    int remaining = findText.Length % charLength;
                    int start = 0;

                    //replace it at first and change the style 
                    string subStr = findText.Substring(start, charLength);
                    _Word.Range replaceRange1 = Document.Content.Find(subStr);

                    ////then, I just paste it where the replacement text is
                    replaceRange1.PasteSpecial(iconIndex, link, placement, displayAsIcon, dataType, iconFileName, iconLabel);

                    for (int i = 1; i < numSegments-1; i++)
                    {
                        start += charLength;
                        subStr = findText.Substring(start, charLength);

                        //Calls a function that doesn't do anything!!! 
                        FindAndReplaceText(Content, subStr, string.Empty);
                    }

                    // the last segment isn't a full 250
                    if (remaining > 0)
                    {
                        start += charLength;
                        subStr = findText.Substring(start, remaining);

                        //Calls a function that doesn't do anything!!! 
                        FindAndReplaceText(Content, subStr, string.Empty);
                    }

                }

                _Word.Range replaceRange = m_Document.Content.Find(findText);
                //replaceRange.AutoFormat();

                //then, I just paste it where the replacement text is
                replaceRange.PasteSpecial(iconIndex, link, placement, displayAsIcon, dataType,
                    iconFileName, iconLabel);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);

                ////it can only handle up to 255 characters
                //string replace1 = findText.Substring(0, findText.Length / 2);
                //string replace2 = findText.Substring(findText.Length / 2);

                //_Word.Range replaceRange = Document.Content.Find(replace1);

                ////then, I just paste it where the replacement text is
                //replaceRange.PasteSpecial(iconIndex, link, placement, displayAsIcon, dataType,
                //    iconFileName, iconLabel);

                //FindAndReplaceText(Content, replace2, string.Empty);
            }
        }

        private void ClearFormattingPrep()
        {
            Document.ShowGrammaticalErrors = false;
            Document.ShowRevisions = false;
            Document.ShowSpellingErrors = false;
        }

        private void PasteWithFormatting(string findText, Range shapeRange = null, bool useDocFormatting = true)
        {
            WdRecoveryType wdRecoveryType;

            if (useDocFormatting)
            {
                wdRecoveryType = WdRecoveryType.wdFormatOriginalFormatting;
            }
            else
            {
                wdRecoveryType = WdRecoveryType.wdFormatPlainText;
            }

            try
            {
                m_Document.Activate();
                ClearFormattingPrep();
                m_WordApp.Selection.Find.ClearFormatting();

                _Word.Range replaceRange = m_Document.Content.Find(findText);

                //then, I just paste it where the replacement text is
                replaceRange.PasteAndFormat(wdRecoveryType);
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                Debug.WriteLine(ex.Message);
                LogError($"{ex.Message}: {ex.StackTrace}");
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                string replace1 = findText.Substring(0, findText.Length / 2);
                string replace2 = findText.Substring(findText.Length / 2);

                _Word.Range replaceRange = Document.Content.Find(replace1);

                replaceRange.PasteAndFormat(wdRecoveryType);

                //IMPORTANT CALLS A FUNCTION THAT DOESN'T DO ANYTHING
                //IMPORTANT 
                FindAndReplaceText(Content, replace2, string.Empty);                
            }
        }

        public void PasteExcelTable()
        {
            //WdRecoveryType wdRecoveryType = WdRecoveryType.wdTableOriginalFormatting;

            //Copy the table
        }

        public void ConditionalUpdate(int percent)
        {
            if (m_UpdaterObj)
            {
                if (percent >= 0 && percent <= 100)
                {
                    UpdateProgressBar(percent);
                }
            }
        }

        public void ConditionalUpdate(string text)
        {
            if (m_UpdaterObj)
            {
                UpdateText(text);
            }
        }

        public void FullReplacement()
        {
            m_UpdaterObj = true;

            ConditionalUpdate("Loading tables . . .");
            //TODO: Wait for event to complete?
            //TODO: I should to this first! 
            ReplaceTables();
            ConditionalUpdate(99);

            ConditionalUpdate("Headers and footers . . .");
            ReplaceHeadersAndFooters();
            ConditionalUpdate(99);

            ConditionalUpdate("Loading text boxes . . .");
            //TODO: Wait for event to complete
            ReplaceTextBoxParagraphs();

            ConditionalUpdate("Loading shapes . . .");
            //TODO: Wait for event to complete?
            ReplaceShapeParagraphs();

            ConditionalUpdate("Loading document body . . .");
            ReplaceMatchingTags();

            ConditionalUpdate(99);

            CloseWordDocument();

            //Do I need to re-apply page numbers after that?
            ConditionalUpdate(100);
        }


        public void ReplaceMatchingTags()
        {
            _Word.Range myRange = m_WordApp.ActiveDocument.Content;
            List<string> xmlStrings = TagConstants.GetXMLMatches(myRange.Text);

            int numMatches = xmlStrings.Count;
            int numComplete = 0;

            foreach (string match in xmlStrings)
            {
                try
                {
                    string cleanMatch = XmlMethods.CleanXmlString(match);
                    XmlDocument xmlDocument = new XmlDocument();
                    xmlDocument.LoadXml(cleanMatch.ToUpper());
                    CreateXmlReplacement(xmlDocument, match);

                    #region Commented out code
                    ////If it's a word document... we just want to replace the range
                    //if (documentType == TagConstants.WORD)
                    //{
                    //    ////maybe I should remove the text, and then insert the range before the words
                    //    //WordInterop wordInterop = replacement as WordInterop;

                    //    //if (!wordInterop.IsOpen)
                    //    //{
                    //    //    wordInterop.OpenWordDocument();
                    //    //}

                    //    //object replaceContent = wordInterop.Content;

                    //    //object weirdTest = "weird test let's see if it works";
                    //    //FindAndReplaceRange(m_WordApp.ActiveDocument.Content, match.Value, weirdTest, documentType);

                    //    //var oh = m_WordApp.ActiveDocument.Content.Text;

                    //    //wordInterop.CloseWordDocument();
                    //    //File.Delete(wordInterop.FilePath);
                    //}
                    //else if (documentType == TagConstants.EXCEL)
                    //{
                    //    //either a string or a _Excel.Range
                    //    //FindAndReplaceRange(myRange, match.Value, replacement, documentType);
                    //}

                    //FindAndReplaceRange(myRange, match.Value, replacement);

                    //if it's a single cell excel document... we just want to replace the text
                    //If it's a multi cell excel document... we want to replace the range
                    #endregion

                    numComplete++;
                    int percent = (int)(((float)numComplete/(numMatches+2))*100.0);
                    ConditionalUpdate(percent);
                }
                catch (XmlException ex)
                {
                    Debug.WriteLine(ex.Message);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);

                    //close the document

                }
            }

            SaveWordDocument();
        }

        public void ReplaceTextBoxParagraphs()
        {
            IList<_Word.Shape> textboxes = m_WordApp.ActiveDocument.TextBoxes();
            int textboxCount = textboxes.Count;
            int i = 0;

            foreach (var textbox in textboxes)
            {
                var textFrame = textbox.TextFrame;

                if (textFrame != null)
                {
                    _Word.Range textRange;

                    try
                    {
                        textRange = textFrame.TextRange;
                    }
                    catch
                    {
                        goto Accumulate;
                    }

                    if (textRange != null)
                    {
                        List<string> xmlStrings = TagConstants.GetXMLMatches(textRange.Text);

                        foreach (string match in xmlStrings)
                        {
                            try
                            {
                                string cleanMatch = XmlMethods.CleanXmlString(match);
                                XmlDocument xmlDocument = new XmlDocument();
                                xmlDocument.LoadXml(cleanMatch.ToUpper());
                                CreateXmlReplacement(xmlDocument, match, textRange);
                            }
                            catch (Exception ex)
                            {
                                Debug.WriteLine(ex.Message);
                            }
                        }
                    }

                //goto
                Accumulate:
                    i++;
                    double percentComplete = (double)i / textboxCount * 100.0;
                    ConditionalUpdate((int)percentComplete);
                }
            }
        }

        public void ReplaceTables()
        {
            Tables tables = m_WordApp.ActiveDocument.Tables;
            int tableCount = tables.Count;

            double percentComplete = 0;
            int numTable = 0;

            foreach (_Word.Table table in tables)
            {
                //Range tableText = table.ConvertToText();
                //double numString = 0;

                // TODO: Iterate through rows and columns in the table
                try
                {
                    //get number of rows
                    int numRows = table.Rows.Count;
                    //get number of columns
                    int numColumns = table.Columns.Count;

                    for (int rowIndex = 0; rowIndex < numRows; rowIndex++)
                    {
                        for (int columnIndex = 0; columnIndex < numColumns; columnIndex++)
                        {
                            _Word.Cell cell = table.Cell(rowIndex, columnIndex);
                            // get the text from the cell

                            Range cellRange = cell.Range;
    
                            List<string> xmlStrings = TagConstants.GetXMLMatches(cellRange.Text);

                            for (int numString = 1; numString <= xmlStrings.Count; numString++)
                            {
                                //TODO: Here it is....  
                                string match = xmlStrings[numString - 1];

                                try
                                {
                                    string cleanMatch = XmlMethods.CleanXmlString(match);
                                    XmlDocument xmlDocument = new XmlDocument();
                                    xmlDocument.LoadXml(cleanMatch.ToUpper());

                                    Range xmlRange = cellRange.Find(match);

                                    //should use the table itself instead...
                                    //CreateXmlReplacement(xmlDocument, cleanMatch, tableText);
                                    //NOTE: CHANGED cleanMatch TO match IN PARAMETERS. MAYBE THAT'S WHY IT WASN'T BEING REPLACED! 
                                    CreateXmlReplacement(xmlDocument, match, xmlRange);
                                }
                                catch (Exception ex)
                                {
                                    //object has been deleted
                                    Debug.Write(ex.Message);
                                }

                                double percentOfStrings = (double)numString / xmlStrings.Count;
                                double tableNumAndPart = (double)percentOfStrings + numTable;
                                double decimalComplete = (double)tableNumAndPart / tableCount;
                                percentComplete = (double)decimalComplete * 100.0;
                                ConditionalUpdate((int)percentComplete);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }

                //tableText.ConvertToTable();

                numTable++;
                percentComplete = (double)numTable / tableCount * 100.0;
                ConditionalUpdate((int)percentComplete);
            }
        }

        public void ReplaceShapeParagraphs()
        {
            var shapes = m_WordApp.ActiveDocument.Shapes;
            int shapeCount = shapes.Count;
            int i = 0;

            foreach (_Word.Shape shape in shapes)
            {
                var textFrame = shape.TextFrame;

                if (textFrame != null && textFrame.HasText != 0)
                {
                    _Word.Range textRange;
                    
                    try
                    {
                        textRange = textFrame.TextRange;
                    }
                    catch
                    {
                        goto Accumulate;
                    }

                    if (textRange != null)
                    {
                        List<string> xmlStrings = TagConstants.GetXMLMatches(textRange.Text);

                        foreach (string match in xmlStrings)
                        {
                            try
                            {
                                string cleanMatch = XmlMethods.CleanXmlString(match);
                                XmlDocument xmlDocument = new XmlDocument();
                                xmlDocument.LoadXml(cleanMatch.ToUpper());
                                CreateXmlReplacement(xmlDocument, cleanMatch, textRange);
                            }
                            catch (Exception ex)
                            {
                                Debug.WriteLine(ex.Message);
                            }
                        }
                    }
                }

                //goto
                Accumulate:
                    i++;
                double percentComplete = (double)i / shapeCount * 100.0;
                ConditionalUpdate((int)percentComplete);
            }
        }

        private void ReplaceRange(Range range)
        {
            List<string> xmlStrings = TagConstants.GetXMLMatches(range.Text);

            foreach (string match in xmlStrings)
            {
                try
                {
                    string cleanMatch = XmlMethods.CleanXmlString(match);
                    XmlDocument xmlDocument = new XmlDocument();
                    xmlDocument.LoadXml(cleanMatch.ToUpper());
                    CreateXmlReplacement(xmlDocument, cleanMatch, range);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }
        }

        public void ReplaceHeadersAndFooters()
        {
            // _Word.Range myRange = m_WordApp.ActiveDocument.Sections.Headers[]
            ConditionalUpdate("Loading headers . . .");

            try
            {
                //Range rng = section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;
                //Word.Fields flds = rng.Fields;
                //foreach (Word.Field fld in flds)

                    // Loop through all sections
                    foreach (Section section in Document.Sections)
                {
                    Document.TrackRevisions = false; //Disable Tracking for the Field replacement operation

                    Range headers = section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;
                    ReplaceRange(headers);

                    //headers.Fields;

                    Range footers = section.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;
                    ReplaceRange(footers);

                    //foreach (HeaderFooter header in section.Headers)
                    //{
                    //    ReplaceRange(header.Range);
                    //}

                    //foreach (HeaderFooter footer in section.Footers)
                    //{
                    //    ReplaceRange(footer.Range);
                    //}
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }

            ConditionalUpdate("Loading footers . . .");
        }

        #region Commented out code
        //public void SlowReplacement()
        //{
        //    var paragraphs = m_Document.Paragraphs;

        //    int openCount = 0;
        //    int closeCount = 0;
        //    bool unfinished = false;

        //    List<string> xmlStrings = new List<string>();

        //    foreach (_Word.Paragraph paragraph in paragraphs)
        //    {
        //        //paragraphs.
        //        _Word.Range range = paragraph.Range;
        //        string text = range.Text;

        //        //need to create struct to track the values of paragraph num and index
        //        FindReplaceableTags(text, ref openCount, ref closeCount, ref xmlStrings, ref unfinished);
        //    }

        //    foreach (string xml in xmlStrings)
        //    {
        //        if (!string.IsNullOrEmpty(xml))
        //        {
        //            string documentType; 

        //            XmlDocument xmlDocument = new XmlDocument();
        //            xmlDocument.LoadXml(xml.ToUpper());
        //            CreateXmlReplacement(xmlDocument, out documentType);
        //        }
        //    }
        //}
        #endregion

        /// <summary>
        /// Get the specified document path
        /// </summary>
        /// <param name="rootNode"></param>
        /// <returns></returns>
        private string GetDocumentPath(XmlNode rootNode)
        {
            string documentPath;
            //bool isReplaceable = false;

            //foreach (XmlNode xmlNode in rootNode.ChildNodes)
            //{
            //    if (xmlNode.Name == TagConstants.REPLACEABLE)
            //    {
            //        isReplaceable = true;
            //        break;
            //    }
            //}

            //if (isReplaceable)
            //{
            //    //TODO: Check if the replaceable is replaceable

            //Get the cell 
            int row = Int32.Parse(rootNode[TagConstants.ROW].InnerText);
            int column = Int32.Parse(rootNode[TagConstants.ROW].InnerText);

            string text = GetExcelSingleCell(rootNode[TagConstants.REPLACEABLE], rootNode[TagConstants.SHEET].InnerText, row, column, rootNode[TagConstants.DOCUMENT_PATH].InnerText, false);

            //replace the Replaceable portion
            RegexOptions ignoreCase = RegexOptions.IgnoreCase;
            documentPath = rootNode[TagConstants.DOCUMENT_PATH].InnerText;
            string filePath = Regex.Replace(documentPath, TagConstants.REPLACEABLE, text, ignoreCase);
            filePath = filePath.Trim();

            //get the document type
            string documentType = rootNode[TagConstants.DOCUMENT_TYPE].InnerText.ToUpper();
            string extension = string.Empty;

            //set the extension
            if (documentType == TagConstants.WORD)
            {
                extension = TagConstants.DOCX;
            }
            else if (documentType == TagConstants.EXCEL)
            {
                extension = TagConstants.XLSX;
            }

            documentPath = m_RootPath + @"\" + filePath + extension;
            //}
            //else
            //{
            //    documentPath = rootNode[TagConstants.DOCUMENT_PATH].InnerText;
            //}

            return documentPath;
        }

        /// <summary>
        /// Creates replaced document path
        /// </summary>
        /// <param name="documentType">Type of document</param>
        /// <param name="fileName">Original document file name</param>
        /// <param name="documentPath">Original document path</param>
        /// <returns></returns>
        private string CreateReplacedDocumentPath(string documentType, string fileName, string documentPath)
        {
            int lastSlashInd = documentPath.LastIndexOf(@"\");
            string replaceFile = fileName.Trim();

            // Add extension based on the document type
            if (documentType == TagConstants.WORD)
            {
                replaceFile += TagConstants.DOCX;
            }
            else if (documentType == TagConstants.EXCEL)
            {
                replaceFile += TagConstants.XLSX;
            }
            else
            {
                //this is a problem
            }

            string replacedPath = documentPath.Substring(0, lastSlashInd + 1) + replaceFile;


            return replacedPath;
        }

        private void CreateXmlReplacement(XmlDocument xmlDocument, string match, Range shapeTextRange = null)
        {
            string documentType = string.Empty;

            try
            {
                //Get the root
                XmlNode rootNode = xmlDocument.DocumentElement;

                //Determine what type of document
                rootNode[TagConstants.DOCUMENT_TYPE].InnerText = rootNode[TagConstants.DOCUMENT_TYPE].InnerText.Trim();
                documentType = rootNode[TagConstants.DOCUMENT_TYPE].InnerText.ToUpper();

                //If no document type is specific, disregard it
                if (documentType == null)
                {
                    return;
                }

                //string documentPath = GetDocumentPath(rootNode);
                string documentPath = rootNode[TagConstants.DOCUMENT_PATH].InnerText;

                try
                {
                    XmlNode replaceNode = XmlMethods.GetXmlNodeChild(TagConstants.REPLACEABLE, rootNode);

                    if (replaceNode != null)
                    {
                        //Get row and column
                        int row = Int32.Parse(replaceNode[TagConstants.ROW].InnerText);
                        int col = Int32.Parse(replaceNode[TagConstants.COLUMN].InnerText);

                        replaceNode[TagConstants.SHEET].InnerText = replaceNode[TagConstants.SHEET].InnerText.Trim();

                        //REMEMBER TO STRIP ALL WHITESPACE BETWEEN REPLACEABLE AND THE SLASH AND/OR TAG
                        string replacePath = replaceNode[TagConstants.DOCUMENT_PATH].InnerText.Trim();

                        // Get the name of the document and replace the documentpath
                        string text = GetExcelSingleCell(replaceNode, replaceNode[TagConstants.SHEET].InnerText, col, row, replacePath, false);
                        string replaceDocPath = CreateReplacedDocumentPath(documentType, text, documentPath); // hmmm here is the issue

                        rootNode[TagConstants.DOCUMENT_PATH].InnerText = documentPath = replaceDocPath;
                    }
                }
                catch
                {

                }
              
                switch (documentType)
                {
                    case TagConstants.WORD:
                        {
                            //Perform the replacement
                            GetWordData(documentPath, match, shapeTextRange);
                            break;
                        }

                    case TagConstants.EXCEL:
                        {
                            rootNode[TagConstants.SHEET].InnerText = rootNode[TagConstants.SHEET].InnerText.Trim();
                            GetExcelData(rootNode, match, shapeTextRange);
                            break;
                        }

                    default:
                        {
                            //Throw an exception
                            throw new Exception("Invalid tag document type tag");
                        }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw ex;
            }
            finally
            {

            }
        }

        public List<int> GetCellsInfo(Dictionary<string, string> pairs)
        {
            if (pairs.ContainsKey(TagConstants.ROW) && pairs.ContainsKey(TagConstants.COLUMN))
            {
                int row, column;

                if (!Int32.TryParse(pairs[TagConstants.ROW], out row))
                {
                    //break out
                }

                if (!Int32.TryParse(pairs[TagConstants.COLUMN], out column))
                {
                    //break out 
                }

                return new List<int>() { row, column };
            }

            if (pairs.ContainsKey(TagConstants.TOP_ROW) && pairs.ContainsKey(TagConstants.BOTTOM_ROW) && 
                pairs.ContainsKey(TagConstants.LEFT_COLUMN) && pairs.ContainsKey(TagConstants.RIGHT_COLUMN))
            {
                int topRow = Int32.Parse(pairs[TagConstants.TOP_ROW]);
                int leftColumn = Int32.Parse(pairs[TagConstants.LEFT_COLUMN]);

                int bottomRow = Int32.Parse(pairs[TagConstants.BOTTOM_ROW]);
                int rightColumn = Int32.Parse(pairs[TagConstants.RIGHT_COLUMN]);

                return new List<int>() { topRow, leftColumn, bottomRow, rightColumn };
            }

            return new List<int>();
        }

        public object GetExcelData(XmlNode rootNode, string match, Range replaceRange = null, bool headerFooter = false)
        {
            Dictionary<string, string> keyValuePairs = new Dictionary<string, string>();

            foreach (XmlNode xmlNode in rootNode.ChildNodes)
            {
                keyValuePairs[xmlNode.Name] = xmlNode.InnerText;
            }

            List<int> cells = GetCellsInfo(keyValuePairs);
           
            if (cells.Count == 0)
            {
                //this is an issue. Alert the user and move on.
            }
            else if (cells.Count == 2)
            {
                bool isDateTime = false;

                if (keyValuePairs.ContainsKey(TagConstants.DATE_FLAG))
                {
                    //int year = ((Int32.Parse(text) / 365) + 1900);
                    //text = DateTime.FromOADate(41060).ToString();
                    isDateTime = true;
                }

                string text = GetExcelSingleCell(rootNode, rootNode[TagConstants.SHEET].InnerText, cells[0], cells[1], keyValuePairs[TagConstants.DOCUMENT_PATH], isDateTime);

                //single replacement
                if (replaceRange == null)
                {
                    //IMPORTANT
                    FindAndReplaceText(Content, match, text);
                }
                else
                {
                    //IMPORTANT 
                    FindAndReplaceText(replaceRange, match, text);
                }


            }
            else if (cells.Count == 4)
            {
                GetExcelMultiCell(rootNode);
                //PasteSpecial(match);

                PasteWithFormatting(match, replaceRange);
            }

            return null;
        }

        public void GetExcelMultiCell(XmlNode rootNode)
        {
            int startRow = Int32.Parse(rootNode[TagConstants.TOP_ROW].InnerText);
            int endRow = Int32.Parse(rootNode[TagConstants.BOTTOM_ROW].InnerText);
            int startCol = Int32.Parse(rootNode[TagConstants.LEFT_COLUMN].InnerText);
            int endCol = Int32.Parse(rootNode[TagConstants.BOTTOM_ROW].InnerText);

            //string searchablePath = GetSearchablePath(rootNode[TagConstants.DOCUMENT_PATH].InnerText);
            string searchablePath = RootPlusFilePath(rootNode[TagConstants.DOCUMENT_PATH].InnerText);

            //ExcelInterop excelInterop = new ExcelInterop(searchablePath);
            LoadWorkbook(searchablePath);
            m_ExcelFileName = searchablePath;

            //excelInterop.LoadWorksheet(rootNode[TagConstants.SHEET].InnerText);
            m_ExcelInterop.LoadWorksheet(rootNode[TagConstants.SHEET].InnerText);

            //excelInterop.CopyRangeToClipboard(startRow, endRow, startCol, endCol);
            m_ExcelInterop.CopyRangeToClipboard(startRow, endRow, startCol, endCol);
        }

        public string RootPlusFilePath(string filePath)
        {
            //filePath.Trim();

            string[] tokens = filePath.Split('\\');

            foreach (string token in tokens)
            {
                token.Trim();
            }

            string temp = string.Empty;

            for (int i = 0; i < tokens.Length - 1; i++)
            {
                temp += tokens[i].Trim() + @"\\";
            }

            temp += tokens[tokens.Length - 1];

            return RootPath + @"\" + temp;
        }

        public string GetExcelSingleCell(XmlNode rootNode, string worksheet, int row, int column, string filePath, bool isDateTime = false)
        {
            string searchablePath = RootPlusFilePath(filePath); //GetSearchablePath(filePath);
            //ExcelInterop excelInterop = new ExcelInterop(searchablePath);

            LoadWorkbook(searchablePath);
            m_ExcelFileName = searchablePath;

            //excelInterop.LoadWorksheet(worksheet);
            m_ExcelInterop.LoadWorksheet(worksheet);

            //string result = excelInterop.ReadCell(row, column);
            string result = m_ExcelInterop.ReadCell(row, column, isDateTime);

            //excelInterop.CloseFile();

            return result;
        }

        public void CloseAll()
        {
            try
            {
                //m_ExcelInterop.Dispose();
                CloseWordDocument();
                m_ExcelInterop.CloseFile();

                //m_WordApp.Quit();
            }
            catch (Exception ex)
            {
                Debug.Write(ex.Message);
            }
        }

        public void QuitWordApp()
        {
            m_WordApp.Quit();
        }

        //public string GetWordPath(XmlNode rootNode)
        //{
        //    //get the data from excel
        //    GetExcelSingleCell(rootNode);

        //    //check a node for the extension
        //    string extension = rootNode[TagConstants.EXTENSION].InnerText;

        //    //fix this
        //    return string.Empty + extension;
        //}

        public static string GenerateRandomString(int length)
        {
            // creating a StringBuilder object()
            StringBuilder str_build = new StringBuilder();
            Random random = new Random();

            char letter;

            for (int i = 0; i < length; i++)
            {
                double flt = random.NextDouble();
                int shift = Convert.ToInt32(Math.Floor(25 * flt));
                letter = Convert.ToChar(shift + 65);
                str_build.Append(letter);
            }
            
            return str_build.ToString();
        }

        public static string CreateRandomFileCopy(string filePath, string extension)
        {
            string fileCopy = string.Empty;

            do
            {
                string randomStr = GenerateRandomString(7);
                fileCopy = filePath;

                if (fileCopy.Length > 0)
                {
                    if (fileCopy[fileCopy.Length - 1] != '\\')
                    {
                        fileCopy += '\\';
                    }
                }

                fileCopy += randomStr + extension;

            } while (File.Exists(fileCopy));

            return fileCopy;
        }

        public void CreateNewTemplatedDocument(string documentPath)
        {


        }

        private string GetSearchablePath(string documentPath)
        {
            string searchablePath;

            if (m_RootPath == string.Empty)
            {
                searchablePath = documentPath;
            }
            else
            {
                if (IsTemplateDocument)
                {
                    searchablePath = documentPath;
                }
                else
                {
                    searchablePath = m_RootPath + @"\" + documentPath;
                }
            }

            return searchablePath;
        }

        /// <summary>
        /// Extract the body of a word document from a document and add it to the main document
        /// </summary>
        /// <param name="rootNode"></param>
        public void GetWordData(string documentPath, string match, Range shapeRange = null)
        {
            //string searchablePath = GetSearchablePath(documentPath);
            string searchablePath = RootPlusFilePath(documentPath);

            //copy the file
            string fileCopyName = CreateRandomFileCopy(Path.GetDirectoryName(searchablePath), Path.GetExtension(documentPath));
            AddFileToDelete(fileCopyName);

            //Open the word document and replace all of the inner tags
            WordInterop wordInterop = new WordInterop(fileCopyName, searchablePath);

            //I GUESS IT"S STILL NOT DONE YET
            //TODO: Replace within the document 
            //wordInterop.ReplaceMatchingTags();

            //not ready to replace shapeRange yet
            CopyPasteWithFormatting(match, wordInterop.Document);

            wordInterop.CloseWordDocument();
            File.Delete(fileCopyName);
        }

        public void LoadWorkbook(string loadFile)
        {
            if (loadFile != m_ExcelFileName)
            {
                DeletePrevious deletePrevious = null;

                if (loadFile != string.Empty)
                {
                    deletePrevious = new DeletePrevious(true, m_ExcelFileName);
                }

                string filePath = Path.GetDirectoryName(loadFile);
                string copyName = CreateRandomFileCopy(filePath, Path.GetExtension(loadFile));


                AddFileToDelete(copyName);

                ExcelFileToDelete = copyName;
                m_ExcelInterop.LoadWorkbook(copyName, loadFile, deletePrevious);
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    CloseWordDocument();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
