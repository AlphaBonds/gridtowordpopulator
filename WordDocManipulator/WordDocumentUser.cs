﻿using MethodResult;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using Microsoft.Office.Tools.Word;
using System.Text.RegularExpressions;
using System.Xml;
using ExcelManipulator;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Office2013.PowerPoint.Roaming;
using Fare;

/// Each JSON Item will have the following fields
/// Type = Word, Excel, WordFileInCell
/// (Relative Path) Document Path 
/// (Excel Only) Sheet = {The name of the sheet to edit}
/// (Excel Only) Cells = {Dictionary of cells to include}
/// (Excel Only) Row Top = {The lowest number row}
/// (Excel Only) Row Bottom = {The highest number row}
/// (Excel Only) Column Left = {Leftmost column}
/// (Excel Only) Column Right = {Rightmost row}

namespace WordDocManipulator
{
    public class WordDocumentUser
    { 
        public static readonly string[] AllowedExtensions = { ".DOCX", ".DOCM"};

        public const string LeftTag = "&lt;";
        public const string RightTag = "&gt;";
        public const string SingleQuote = "&apos;";
        public const string DoubleQuote = "&quot;";
        public const string AndChar = "&amp";

        public static readonly Dictionary<string, string> ReplacementDictionary = new Dictionary<string, string>()
        {
            {LeftTag, "<" },
            {RightTag, ">" },
            {SingleQuote, @"'"},
            {DoubleQuote, "\"" },
            {AndChar, "&" }
        };

        public static readonly List<string> BadTags = new List<string>()
        {
            "w:t",
            "w:p",
            "w:r"
        };

        public const string AnySpacings = @"\s*";
        public const string AnyNonTerminatorChars = ".*";
        public const string NotCaseSensitive = "(?i)";

        public static readonly string ReplaceableOpenTag = $"{LeftTag}Replaceable{RightTag}";
        public static readonly string ReplaceableCloseTag = $"{LeftTag}/Replaceable{RightTag}";

        public static readonly string DocumentPathOpenTag = $"{LeftTag}DocumentPath{RightTag}";
        public static readonly string DocumentPathCloseTag = $"{LeftTag}/DocumentPath{RightTag}";

        public const string ROW = "ROW";
        public const string COLUMN = "COLUMN";

        public const string LEFT_COLUMN = "LEFTCOLUMN";
        public const string RIGHT_COLUMN = "RIGHTCOLUMN";

        public const string TOP_ROW = "TOPROW";
        public const string BOTTOM_ROW = "BOTTOMROW";

        public const string WORKSHEET = "Worksheet";
        public const string EXCEL_TYPE = "ExcelType";
        public const string EXTENSION = "Extension";
        public const string REPLACE_PATH = "ReplacePath";
        public const string REPLACEABLE = "Replaceable";
        public const string DOCUMENT_PATH = "DocumentPath";

        private WordprocessingDocument m_MainWordDocument;
        //private MemoryStream m_MemoryStream; 

        public WordDocumentUser()
        {
        }

        public Stream GetDocStream()
        {
            return m_MainWordDocument.MainDocumentPart.GetStream();
           
        }

        public string GetDocBodyText()
        {
            string docText = string.Empty;
            using (StreamReader sr = new StreamReader(GetDocStream()))
            {
                docText = sr.ReadToEnd();
            }

            return docText;
        }

        public void RenameWordDoc(string currName, string newName)
        {
            File.Move(currName, newName);
        }

        /// <summary>
        /// Read all content from the word document into the word document data member
        /// </summary>
        /// <param name="filePath"></param>
        public void MakeWordDoc(string filePath)
        {
            //Document document = Globals.
            m_MainWordDocument = WordprocessingDocument.Create(filePath, WordprocessingDocumentType.Document, true);
            m_MainWordDocument.Close();
        }

        public ReturnValue LoadWordDoc(string filePath)
        {
            ReturnValue returnValue = new ReturnValue();
            m_MainWordDocument = WordprocessingDocument.Open(filePath, true);
            //m_MainWordDocument.Close();

            return returnValue;
        }

        public ReturnValue InsertText(string text)
        {
            ReturnValue returnValue = new ReturnValue();

            return returnValue;
        }

        public ReturnValue InsertText(List<string> text)
        {     
            ReturnValue returnValue = new ReturnValue();

            return returnValue;
        }

        public static string GetWordData(string filePath)
        {
            //note we can do replacements for all internal docs too, but what if it is recursive
            //may need to create dictionary

            string result = string.Empty;

            WordDocumentUser wordDocumentUser = new WordDocumentUser();
            wordDocumentUser.LoadWordDoc(filePath);
            wordDocumentUser.ReplaceWords();

            result = wordDocumentUser.GetDocBodyText();

            return result;
        }

        private static Dictionary<string, string> GetXmlUserDictionary(XmlNodeList xmlNodeList)
        {
            Dictionary<string, string> keyValuePairs = new Dictionary<string, string>();

            foreach (XmlNode node in xmlNodeList)
            {
                keyValuePairs[node.Name] = node.InnerText;
            }

            return keyValuePairs;
        }

        private static string GetSingleCellExcelText(XmlNodeList xmlNodeList, string fileFull = "")
        {

            Dictionary<string, string> xmlPairs = GetXmlUserDictionary(xmlNodeList);

            //get the info from the file                
            ExcelDocumentUser excelDocumentUser = new ExcelDocumentUser();

            if (fileFull == string.Empty)
            {
                if (xmlPairs.ContainsKey(DOCUMENT_PATH))
                {
                    if (xmlPairs.ContainsKey(EXTENSION))
                    {
                        //ReturnValue ret = excelDocumentUser.LoadExcelDoc(derivePath);
                        ReturnValue ret = new ReturnValue();

                        if (!ret.Success)
                        {

                        }
                    }
                }
            }
            else
            {
                //ReturnValue ret = excelDocumentUser.LoadExcelDoc(fileFull);
                ReturnValue ret = new ReturnValue(); 

                if (!ret.Success)
                {

                }
            }

            if (xmlPairs.ContainsKey(ROW) && xmlPairs.ContainsKey(COLUMN))
            {
                if (xmlPairs.ContainsKey(WORKSHEET))
                {
                    string text = excelDocumentUser.GetCellValue(xmlPairs[DOCUMENT_PATH], xmlPairs[WORKSHEET], xmlPairs[COLUMN], Convert.ToInt32(xmlPairs[ROW]));

                    // if we are actually using the word document associated with this instead of the actual value
                    if (fileFull == string.Empty)
                    {
                        return text + xmlPairs[EXTENSION];
                    }
                    else 
                    {
                        return text;
                    }
                }
                
                //else
                //{

                //}
            }
            else
            {

            }

            return string.Empty;
        }

        private static string GetCellRangeExcelText(XmlNodeList xmlNodeList, string filePath)
        {
            string result = string.Empty; 

            Dictionary<string, string> xmlPairs =  GetXmlUserDictionary(xmlNodeList);
            ExcelDocumentUser excelDocumentUser = new ExcelDocumentUser();

            result = excelDocumentUser.GetMultipleCellValues(xmlPairs[DOCUMENT_PATH], xmlPairs[WORKSHEET], xmlPairs[LEFT_COLUMN], 
                Convert.ToInt32(xmlPairs[TOP_ROW]), xmlPairs[RIGHT_COLUMN], Convert.ToInt32(xmlPairs[BOTTOM_ROW]));

            return result;
        }

        public static string ReplaceInstructions(XmlDocument xmlDocument)
        {
            string result = string.Empty;

            XmlElement root = xmlDocument.DocumentElement;

            XmlNodeList pathNode = root.GetElementsByTagName(DOCUMENT_PATH);
            string path = pathNode[0].InnerText;
            string fileName = Path.GetFileName(path);

            if (fileName == REPLACE_PATH)
            {
                XmlNode innerRoot = pathNode[0].FirstChild;
                XmlNodeList innerNodes = innerRoot.ChildNodes;

                string docName = GetSingleCellExcelText(innerNodes);

                docName = Path.GetDirectoryName(path) + "\\" + docName;
                //now get the info

                return GetWordData(docName);  
            }
            else
            {
                string ext = Path.GetExtension(path);

                if (AllowedExtensions.Contains(ext))
                {
                    return GetWordData(path);
                }
                else if (ExcelDocumentUser.AllowedExtentions.Contains(ext))
                {
                    XmlNodeList nodes = pathNode[0].ChildNodes;

                    if (nodes[0].Name == EXCEL_TYPE)
                    {
                        string convertType = nodes[0].InnerText;

                        if (convertType == ExcelDocumentUser.SINGLE_CELL)
                        {
                            result = GetSingleCellExcelText(nodes, path);
                        }
                        else if (convertType == ExcelDocumentUser.CELL_RANGE)
                        {
                            result = GetCellRangeExcelText(nodes, path);
                        }
                        else
                        {
                            //problem
                        }

                    }
                    else
                    {
                        //not good
                    }                 
                }
            }

            return result;
        }

        public static string CleanInstructionXml(string instructionXml)
        {
            foreach (var key in ReplacementDictionary.Keys)
            {
                instructionXml = instructionXml.Replace(key, ReplacementDictionary[key]);
            }

            //List<string> elements = new List<string>();
            string elements = string.Empty;

            int currInd = 0;

            while (currInd < instructionXml.Length - 1)
            {
                //we need to parse this differently......
                // keep iterating through until we have matching tags

                int firstOpen = instructionXml.IndexOf('<', currInd);

                if (firstOpen == -1)
                {
                    break;
                }

                int firstClose = instructionXml.IndexOf('>', firstOpen) + 1;

                if (firstClose == -1)
                {
                    break;
                }

                if (currInd < firstOpen)
                {
                    //elements.Add(instructionXml.Substring(currInd, firstOpen - currInd));
                    elements += instructionXml.Substring(currInd, firstOpen - currInd);
                }

                string tag = instructionXml.Substring(firstOpen, firstClose - firstOpen);

                if (tag.Contains("Replaceable") || tag.Contains("DocumentPath")) 
                {
                    //go to the matching tag and capture everything in between
                    //elements.Add(tag);
                    elements += tag;
                }

                currInd = firstClose;
            }

            return elements;
        }

        public ReturnValue ReplaceXml(string filePath)
        {
            ReturnValue returnValue = LoadWordDoc(filePath);

            if (!returnValue.Success)
            {
                return returnValue;
            }

            returnValue = ReplaceWords();
            m_MainWordDocument.Close();

            return returnValue;
        }

        public ReturnValue ReplaceWords()
        {
            ReturnValue returnValue = new ReturnValue();
            string docText = string.Empty;
            //@"  {testing:test\s*.*\s*}";
            string pattern = $"{NotCaseSensitive}{ReplaceableOpenTag}{AnySpacings}{AnyNonTerminatorChars}{AnySpacings}{ReplaceableCloseTag}";
            //string pattern = $"{NotCaseSensitive}{FakeReplaceableOpenTag}{AnySpacings}{AnyNonTerminatorChars}{AnySpacings}{FakeReplaceableCloseTag}"; //@"  {testing:test\s*.*\s*}";

            using (StreamReader sr = new StreamReader(m_MainWordDocument.MainDocumentPart.GetStream()))
            {
                docText = sr.ReadToEnd();
                MatchCollection matches = Regex.Matches(docText, pattern);

                foreach (Match match in matches)
                {
                    string instructionXml = match.Value;
                    int startIndex = match.Index;
                    int endIndex = startIndex + match.Length;

                    instructionXml = CleanInstructionXml(instructionXml);

                    //Now create the xml document and parse it
                    XmlDocument xmlDocument = new XmlDocument();
                    xmlDocument.LoadXml(instructionXml);


                    
                    //traverse the tree

                }
            }



            return returnValue;
        }

        public ReturnValue ReplaceVariables()
        {
            ReturnValue returnValue = new ReturnValue();

           //int i = 0;
            foreach (var paragraph in m_MainWordDocument.MainDocumentPart.Document.Body)
            {
                while (paragraph.HasChildren)
                {
                   
                }
            }


            return returnValue;
        }
    }
}
