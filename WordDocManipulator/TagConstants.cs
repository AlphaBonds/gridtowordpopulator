﻿using DocumentFormat.OpenXml.Drawing.Charts;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace WordDocManipulator
{
    public static class TagConstants
    {
        public static readonly string[] AllowedExtensions = { ".DOCX", ".DOCM" };

        public const string LeftTag = "&lt;";
        public const string RightTag = "&gt;";
        public const string SingleQuote = "&apos;";
        public const string DoubleQuote = "&quot;";
        public const string AndChar = "&amp";

        public static readonly Dictionary<string, string> ReplacementDictionary = new Dictionary<string, string>()
        {
            {LeftTag, "<" },
            {RightTag, ">" },
            {SingleQuote, @"'"},
            {DoubleQuote, "\"" },
            {AndChar, "&" }
        };

        public static readonly List<string> BadTags = new List<string>()
        {
            "w:t",
            "w:p",
            "w:r"
        };

        public const string AnySpacings = @"\s*";
        public const string AnyNonTerminatorChars = ".*";
        public const string NotCaseSensitive = "(?i)";

        //public static readonly string ReplaceableOpenTag = $"{LeftTag}{REPLACEABLE}{RightTag}";
        //public static readonly string ReplaceableCloseTag = $"{LeftTag}/{REPLACEABLE}{RightTag}";
        public static readonly string ReplaceableOpenTag = $"<{REPLACEABLE}>";
        public static readonly string ReplaceableCloseTag = $"</{REPLACEABLE}>";

        public static readonly string DocumentPathOpenTag = $"{LeftTag}{DOCUMENT_PATH}{RightTag}";
        public static readonly string DocumentPathCloseTag = $"{LeftTag}/{DOCUMENT_PATH}{RightTag}";

        public const string ROW = "ROW";
        public const string COLUMN = "COLUMN";

        public const string LEFT_COLUMN = "LEFTCOLUMN";
        public const string RIGHT_COLUMN = "RIGHTCOLUMN";

        public const string TOP_ROW = "TOPROW";
        public const string BOTTOM_ROW = "BOTTOMROW";

        public const string SHEET = "SHEET";
        public const string EXCEL_TYPE = "EXCELTYPE";
        public const string EXTENSION = "EXTENSION";
        public const string REPLACE_PATH = "REPLACEPATH";
        public const string REPLACEABLE = "REPLACEABLE";
        public const string DOCUMENT_PATH = "DOCUMENTPATH";
        public const string DATE_FLAG = "DATE";

        public const string DOCUMENT_TYPE = "DOCUMENTTYPE";

        public const string WORD = "WORD";
        public const string EXCEL = "EXCEL";

        public const string DOCX = ".DOCX";
        public const string XLSX = ".XLSX";

        public static readonly string ReplaceableTagRegex = $"{NotCaseSensitive}{ReplaceableOpenTag}{AnySpacings}{AnyNonTerminatorChars}{AnySpacings}{ReplaceableCloseTag}";
        public static readonly string ReplaceableOpenTagRegex = $"{NotCaseSensitive}{ReplaceableOpenTag}";
        public static readonly string ReplaceableCloseTagRegex = $"{NotCaseSensitive}{ReplaceableCloseTag}";

        private static void AddRange(List<Match> matchCollection, MatchCollection addable, int i)
        {
            for (; i < addable.Count; i++)
            {
                matchCollection.Add(addable[i]);
            }
        }

        private static List<Match> SortMatchCollections(MatchCollection collection1, MatchCollection collection2)
        {
            List<Match> matchCollection = new List<Match>();

            int i = 0;
            int j = 0;

            while (i < collection1.Count && j < collection2.Count)
            {
                if (collection1[i].Index < collection2[j].Index)
                {
                    matchCollection.Add(collection1[i]);
                    i++;
                }
                else
                {
                    matchCollection.Add(collection2[j]);
                    j++;
                }
            }    
            
            if (i < collection1.Count)
            {
                AddRange(matchCollection, collection1, i);
            }
            else //if (j < collection2.Count)
            {
                AddRange(matchCollection, collection2, j);
            }

            return matchCollection;
        }

        public static List<string> GetXMLMatches(string text)
        {
            List<string> xmlRegexMatches = new List<string>();

            RegexOptions regexOptions = RegexOptions.IgnoreCase;
            MatchCollection opens = Regex.Matches(text, ReplaceableOpenTag, regexOptions);
            MatchCollection closes = Regex.Matches(text, ReplaceableCloseTag, regexOptions);

            List<Match> matchCollection = SortMatchCollections(opens, closes);

            int openCount = 0;
            int closeCount = 0;
            bool started = false;

            int startInd = 0;

            foreach (Match match in matchCollection)
            {
                if (match.Value.ToUpper() == ReplaceableOpenTag)
                {
                    openCount++;

                    if (!started)
                    {
                        startInd = match.Index;
                        started = true;
                    }
                }
                else
                {
                    closeCount++;

                    if (openCount == closeCount)
                    {
                        int endInd = match.Index + match.Length;

                        string result = text.Substring(startInd, endInd-startInd);
                        xmlRegexMatches.Add(result);

                        started = false;
                    }
                }
            }

            return xmlRegexMatches;
        }
    }
}
