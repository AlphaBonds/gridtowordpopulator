﻿using ExcelManipulator;
using MethodResult;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using WordDocManipulator;

namespace GridToWordPopulator
{
    public partial class StructureBuilder : UserControl
    {
        public StructureBuilder()
        {
            //InitializeComponent();
        }

        public static void BuildStructure(string path)
        {          
            XmlDocument xmlDocument = MakeDirectoryStructure(path);
            xmlDocument.Save(@"Testfile.xml");
        }

        public static ReturnValue CreateStructureFromXmlDoc(string xmlDoc)
        {
            ReturnValue ret = new ReturnValue();

            //XmlDocument xmlDocument = MakeDirectoryStructure(xmlDoc)

            return ret;
        }

        public static XmlDocument MakeDirectoryStructure(string filePath)
        {
            XmlDocument xmlDocument = new XmlDocument();
            XmlNode rootNode = xmlDocument.CreateElement("Root");

            if (DirectoryStructureValidator.ValidatePath(filePath))
            {
                DirectoryExplorer(xmlDocument, rootNode, filePath);
            }

            xmlDocument.AppendChild(rootNode);

            return xmlDocument;
        }

        public static bool IsDirectory(string filePath)
        {
            // get the file attributes for file or directory
            FileAttributes attr = File.GetAttributes(filePath);

            //detect whether its a directory or file
            if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
            {
                return true;
            }
            else
            {
                //TODO: Check if the type is valid
                return false;
            }
        }

        public static void DirectoryExplorer(XmlDocument xmlDocument, XmlNode rootNode, string filePath)
        {
            foreach (string s in Directory.GetDirectories(filePath))
            {
                XmlNode node = xmlDocument.CreateElement("Path");
                XmlNode name = xmlDocument.CreateElement("Name");
                name.InnerText = Path.GetFileName(s);
                node.AppendChild(name);

                DirectoryExplorer(xmlDocument, node, s);                 

                rootNode.AppendChild(node);
            }

            uint numDocs = 0, numExcel = 0;

            foreach (string s in Directory.GetFiles(filePath))
            {
                string extension = Path.GetExtension(s).ToUpper();

                if (ExcelDocumentUser.AllowedExtentions.Contains(extension))
                {
                    numExcel++;
                }
                else if (WordDocumentUser.AllowedExtensions.Contains(extension))
                {
                    numDocs++;
                }
            }

            XmlNode docs = xmlDocument.CreateElement("WordDocuments");
            docs.InnerText = numDocs.ToString();
            rootNode.AppendChild(docs);
            XmlNode excels = xmlDocument.CreateElement("ExcelDocuments");
            excels.InnerText = numExcel.ToString();
            rootNode.AppendChild(excels);

            //foreach (string s in Directory.GetFiles(filePath))
            //{
            //    XmlNode node = xmlDocument.CreateElement("Path");
            //    XmlNode name = xmlDocument.CreateElement("Name");
            //    name.InnerText = Path.GetFileName(s);
            //    node.AppendChild(name);
            //    //XmlNode parsing = xmlDocument.CreateElement("Parsing");
            //    //parsing.InnerText = "";
            //    //node.AppendChild(parsing);

            //    rootNode.AppendChild(node);
            //}
        }
    }
}
