﻿using System;
using System.IO;
using System.Windows.Forms;
using WordDocManipulator;

namespace GridToWordPopulator
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            ////string testPath = @"C:\Users\klingerm\Downloads\TEMPLATES - DO NOT DELETE (4) (1)\TEMPLATES - DO NOT DELETE\9. Report Word Format\Moyer Realty Sample Appraisal Report - (Front Half).docx";
            //const string fileName = @"C:\GIT_ROOT\gridtowordpopulator\TempResources\TEMPLATES - DO NOT DELETE (4)\TEMPLATES - DO NOT DELETE\9. Report Word Format\Moyer Realty Sample Appraisal Report - (Front Half).docx";

            //const string fileName = @"C:\GIT_ROOT\gridtowordpopulator\TempResources\TEMPLATES - DO NOT DELETE (4)\TEMPLATES - DO NOT DELETE\10. Excel Format\Moyer Realty Excel Template.xlsx";
            //const string sheetName = "Reporting Information";

            //WordDocumentUser wordDocumentUser = new WordDocumentUser();
            ////wordDocumentUser.LoadWordDoc(fileName);
            ////wordDocumentUser.ReplaceVariables();
            ////wordDocumentUser.ReplaceWords(); 
            //wordDocumentUser.ReplaceXml(fileName);

           
            ////string text = excelDocumentUser.GetMultipleCellValues(fileName, "Reporting Information", "B", 1, "2", 3)
            ////var test = ExcelDocumentUser.IncrementColumn("ZZZ");
            //string startCol = "A";
            //int startRow = 1;
            //string endCol = "C";
            //int endRow = 5;

            //string text = excelDocumentUser.GetMultipleCellValues(fileName, sheetName, startCol, startRow, endCol, endRow);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Converter());
        }
    }
}
