﻿using MethodResult;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using WordDocManipulator;
using Instructions;
using System.Diagnostics;
using System.Threading;

namespace GridToWordPopulator
{
    public partial class Converter : Form
    {
        public delegate void SetControlText(string text);
        public SetControlText SetFeedbackText;    
        public delegate string GetControlText();
        public GetControlText GetClientDirText;
        public GetControlText GetTemplateText;
        private static InstructionView m_InstructionView;
        //private static bool m_Initialized = false;
        public const string WordProcesses = "WINWORD";  
        public const string ExcelProcesses = "EXCEL";
        private Dictionary<string, bool> FilesToDelete;

        private static ManualResetEventSlim m_ManualResetEvent = new ManualResetEventSlim(false);

        public Converter()
        {
            InitializeComponent();

            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.OptimizedDoubleBuffer, true);

            GetClientDirText = GetClientInformationDirectoryText;
            GetTemplateText = GetTemplateFileText;

            m_InstructionView = new InstructionView();
            m_InstructionView.ResumeForm += ResumeForm;
            m_InstructionView.Dock = DockStyle.Fill;
            this.Controls.Add(m_InstructionView);
            UpdateDateTimeLbl();

            FilesToDelete = new Dictionary<string, bool>();
        }

        ~Converter()
        {
            //Delete all temporary files
            DeleteGeneratedFiles();
        }

        public void UpdateDateTimeLbl()
        {
            if (m_DateTimeLbl.InvokeRequired)
            {
                m_DateTimeLbl.Invoke(new Action(() => m_DateTimeLbl.Text = DateTime.Now.ToString()));
            }
            else
            {
                m_DateTimeLbl.Text = DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss tt");
            }
        }

        /// <summary>
        /// Add temporary files to list of files to delete
        /// </summary>
        /// <param name="filePath"></param>
        private void AddToFilesToDelete(string filePath)
        {
            FilesToDelete.Add(filePath, true);
        }

        private void DeleteGeneratedFiles()
        { 
            TerminateProcesses(ExcelProcesses);

            foreach (string file in FilesToDelete.Keys)
            {
                try
                {
                    //Force quit them all
                    File.Delete(file);
                }
                catch
                {
                    Debug.WriteLine($"File does not exist : {file}");
                }
            }

            FilesToDelete.Clear();
        }

        public string CreateTempFileName(string filePath)
        {
            string path = Path.GetDirectoryName(filePath);
            string nameNoExt = Path.GetFileNameWithoutExtension(filePath);
            string extension = Path.GetExtension(filePath);
            uint i = 0;
            bool keepTrying = true;
            string tempPath = path + @"\" + nameNoExt + i.ToString() + extension;

            while (keepTrying)
            {               
                if (File.Exists(tempPath))
                {
                    i++;
                }
                else
                {
                    break;
                }

                tempPath = path + @"\" + nameNoExt + i.ToString() + extension;
            }

            return tempPath;
        }

        private void WriteDocument(string filePath, ReturnValue returnValue)
        {
            WordDocumentUser wordDocumentUser = new WordDocumentUser();

            string tempPath = CreateTempFileName(filePath);
            File.Copy(filePath, tempPath);
            wordDocumentUser.LoadWordDoc(tempPath);
            wordDocumentUser.ReplaceVariables();

            // write everything to the document

            //rename the temp path
            wordDocumentUser.RenameWordDoc(tempPath, filePath);
        }

        private void LoadParserStructure()
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(@"C:\Users\klingerm\source\repos\GridToWordPopulator\GridToWordPopulator\DirectoryParserStructure.xml");

            //Need to validate
        }

        private bool IsExcelFile(string path)
        {
            return true;
        }

        private void InvalidInput(string message)
        {
            if (m_FeedbackLbl.InvokeRequired)
            {
                m_FeedbackLbl.Invoke(new Action(() => InvalidInput(message)));
            }
            else
            {
                m_FeedbackLbl.Text = message;
            }
        }

        private bool ValidateExcelFile(string filePath)
        {
            return true;
        }

        private bool ValidateTemplate(string filePath)
        {
            return true;
        }

        private void m_SelectFileBtn_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog fileBrowser = new OpenFileDialog()
            {
                Filter = "Word documents(*.docx) | *.docx",
                Title = "Select the Word document template file to derive data from",
                FileName = ""
            })
            {
                //fileBrowser .= "Select the Word document template file to derive data from";

                if (fileBrowser.ShowDialog() == DialogResult.OK)
                {
                    //string selectedPath = fileBrowser.FileName;

                    //if (IsExcelFile(selectedPath))
                    //{
                        m_TemplateFileTxtBox.Text = fileBrowser.FileName;
                    //}
                    //else
                    //{
                    //    InvalidInput(Strings.InvalidInputPath);
                    //}
                }
            }
        }

        private void SelectTemplateDocumentBtn_Click(object sender, EventArgs e)
        {

        }

        private void SelectClientInfoDirBtn_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog folderBrowser = new FolderBrowserDialog())
            {
                folderBrowser.Description = "Select the folder to output the new file to.";

                if (folderBrowser.ShowDialog() == DialogResult.OK)
                {
                    string selectedPath = folderBrowser.SelectedPath;

                    //if (IsExcelFile(selectedPath))
                    //{
                        m_ClientInformationDirectory.Text = folderBrowser.SelectedPath;
                    //}
                    //else
                    //{
                    //    InvalidInput(Strings.InvalidOutputFolder);
                    //}
                }
            }
        }

        private string ListToDelimitedString(List<string> items, string delimiter)
        {
            if (items.Count == 0)
            {
                return string.Empty;
            }

            string result = string.Empty;

            for (int i = 0; i < items.Count - 1; i++)
            {
                result += items[i] + delimiter + ' ';
            }

            result += items[items.Count - 1];

            return result;
        }


        private void m_CreationTable_Paint(object sender, PaintEventArgs e)
        {

        }

        private void m_MainLayout_Paint(object sender, PaintEventArgs e)
        {

        }

        /// <summary>
        ///Determines whether it is okay to write to this file location. Prompts user if that file already exists.
        /// </summary>
        /// <param name="filePath">file path to write to</param>
        /// <param name="returnValue"></param>
        /// <returns>        
        /// Checks to see if there is a file in the filepath. If there is not, it returns true;
        /// If there is, it prompts to the user about whether or not to overwrite. 
        /// If the user chooses to overwrite, it returns true, Otherwise false;
        /// </returns>
        private bool OverwriteDuplicateFile(string filePath, ReturnValue returnValue)
        {
            if (File.Exists(filePath))
            {
                string title = Strings.OverwriteFile;
                string text = Strings.OverwritePrompt;
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result = MessageBox.Show(text, title, buttons);

                if (result == DialogResult.No || result == DialogResult.Cancel)
                {
                    return false;
                }
                else
                {
                    //write the document
                    WriteDocument(filePath, returnValue);

                    if (returnValue.Success)
                    {

                    }
                    else
                    {

                    }
                }
            }

            return true;
        }

        //private bool ValidateInputs()
        //{
        //    List<string> missingFields = new List<string>();

        //    if (m_TemplateFileTxtBox.Text == string.Empty)
        //    {
        //        missingFields.Add(Strings.InputFilePath);
        //    }
        //    if (m_ClientInformationDirectory.Text == string.Empty)
        //    {
        //        missingFields.Add(Strings.OutputFolderPath);
        //    }
        //    if (missingFields.Count > 1)
        //    {
        //        InvalidInput(Strings.MissingInputs + ListToDelimitedString(missingFields, ",") + '.');
        //    }
        //    else if (missingFields.Count == 1)
        //    {
        //        InvalidInput(Strings.MissingInputs + missingFields[0] + '.');
        //    }
        //    //Valid input
        //    else
        //    {
        //        ReturnValue returnValue = new ReturnValue();
        //        bool overwrite = OverwriteDuplicateFile(m_ClientInformationDirectory.Text + '\\' + m_FileCreationName.Text, returnValue);
        //    }

        //    return true;
        //}

        public void GenerateReport()
        {

            //Should we also add a stop button?        

            try
            {
                UpdateText("Loading...");
                ProgressBarVisible = true;

                if (GetTemplateFileText() == string.Empty)
                {
                    UpdateText("Missing template.");
                    return;
                }
                else if (GetClientInformationDirectoryText() == string.Empty)
                {
                    UpdateText("Missing client information directory.");
                    return;
                }

                //string extension = Path.GetExtension(m_TemplateFileTxtBox.Text);
                string extension = ".docx";
                string path = GetTemplateFileText();
                string dirName = Path.GetDirectoryName(path);
                //string path = Path.GetDirectoryName(q);

                //Name of the file generated from copying the original template
                string copyName = WordInterop.CreateRandomFileCopy(dirName, extension);
                AddToFilesToDelete(copyName);
                WordInterop wordInterop = new WordInterop(copyName, path);
                wordInterop.AddFileToDelete = AddToFilesToDelete;
                wordInterop.UpdateProgressBar = UpdateProgressBar;
                wordInterop.UpdateText = UpdateText;

                wordInterop.RootPath = GetClientInformationDirectoryText();
                wordInterop.IsTemplateDocument = true;

                //wordInterop.ReplaceMatchingTags();
                wordInterop.FullReplacement();

                //IMPORTANT: AT THIS POINT, THE BEST POSSIBLE OPTION SEEMS LIKE CREATING A NEW TABLE AND GETTING RID OF THE OLD ONE.

                //copy the save file to another location
                string tempSaveFile = WordInterop.CreateRandomFileCopy(dirName, extension);
                AddToFilesToDelete(tempSaveFile);
                File.Copy(copyName, tempSaveFile);

                WordInterop wordInteropFinal = new WordInterop(tempSaveFile);
                wordInteropFinal.AddFileToDelete = AddToFilesToDelete;
                wordInteropFinal.ShowDialog(true);
                wordInteropFinal.SaveAsWordDocument(showDialog: true);
                wordInteropFinal.ShowDialog(false);

                wordInterop.CloseAll();
                wordInterop.QuitWordApp();
                wordInterop.UnitializeApp();
                wordInterop = null;
                //m_WordInterop = null;

                //determine where they created the file by returning string from SaveASWordDocument
                //NewFileCreatedText(fullName);

                UpdateText("Complete.");
                DeleteGeneratedFiles();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An exception occured that caused the program to fail: {ex.Message}");
                DeleteGeneratedFiles();
                Application.Exit();
            }
        }

        /// <summary>
        /// User clicks button to create the new file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_CreateBtn_Click(object sender, EventArgs e)
        {
            ButtonsEnabled(false);

            new Thread(delegate ()
            {
                GenerateReport();
                ButtonsEnabled(true);
            }).Start();
        }

        private string SaveWordDocAs(string copyName, WordInterop wordInterop, bool showDialog = false)
        {
            wordInterop.LoadWordDocument(copyName);
            wordInterop.SaveAsWordDocument(showDialog);

            string fullName = wordInterop.GetFileName();

            // if the file created is different from the default name, delete it
            if (fullName != copyName)
            {
                File.Delete(copyName);
            }

            wordInterop.CloseWordDocument();

            return fullName;
        }

        private void NewFileCreatedText(string fileName)
        {  
            m_FeedbackLbl.Text = $"New file created: {fileName}";
        }

        public void ButtonsEnabled(bool enabled)
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() => ButtonsEnabled(enabled)));
            }
            else
            {
                m_SelectClientInfoDirBtn.Enabled = m_SelectTemplateFileBtn.Enabled = m_CreateBtn.Enabled = m_WordTerminateBtn.Enabled = m_ExcelTerminate.Enabled = m_DirectoryMatchCheckBx.Enabled = enabled;
            }
        }

        public void UpdateProgressBar(int percentComplete)
        {
            //TODO WATCH THIS

            m_ManualResetEvent.Reset();

            if (m_ProgressBar.InvokeRequired)
            {
                m_ProgressBar.Invoke(new Action(() => UpdateProgressBar(percentComplete)));
            }
            else
            {
                try
                {
                    m_ProgressBar.Value = percentComplete;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
                finally
                {

                }
            }

            m_ManualResetEvent.Set();
        }

        public string GetTemplateFileText()
        {
            if (m_TemplateFileTxtBox.InvokeRequired)
            {
                return m_TemplateFileTxtBox.Invoke(GetTemplateText).ToString();
            }
            else
            {
                return m_TemplateFileTxtBox.Text;
            }     
        }

        public string GetClientInformationDirectoryText()
        {
            if (m_ClientInformationDirectory.InvokeRequired)
            {
                return m_ClientInformationDirectory.Invoke(GetClientDirText).ToString();
            }
            else
            {
                return m_ClientInformationDirectory.Text;
            }
        }

        public void UpdateText(string text)
        {
            m_ManualResetEvent.Reset();

            if (m_FeedbackLbl.InvokeRequired)
            {
                m_FeedbackLbl.Invoke(new Action(() => UpdateText(text)));
            }
            else
            {
                try
                {
                    m_FeedbackLbl.Text = text;
                }
                catch
                {

                }
                finally
                {
                }
            }

            m_ManualResetEvent.Set();
        }

        public bool ProgressBarVisible
        {
            set
            {
                if (m_ProgressBar.InvokeRequired)
                {
                    m_ProgressBar.Invoke(new Action(() => ProgressBarVisible = true));
                }
                else
                {
                    m_ProgressBar.Visible = value;
                }
            }
        }

        private void ResumeForm()
        {
            m_MainLayout.Show();
            m_InstructionView.Hide();
        }

        private void InstructionButton_Click(object sender, EventArgs e)
        {
            m_MainLayout.Hide();
            m_InstructionView.Show();
        }

        private void m_FeedbackLbl_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void m_DateTimeTimer_Tick(object sender, EventArgs e)
        {
            UpdateDateTimeLbl();
        }

        private static void TerminateProcesses(string processName)
        {
            Process[] processes = Process.GetProcessesByName(processName); //I can also do this by machine name

            foreach (Process p in processes)
            {
                p.Kill();
            }
        }

        private void m_WordTerminate_Click(object sender, EventArgs e)
        {
            TerminateProcesses(WordProcesses);
        }

        private void m_ExcelTerminate_Click(object sender, EventArgs e)
        {
            TerminateProcesses(ExcelProcesses);
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            DeleteGeneratedFiles();
            //Environment.Exit(0);
            Application.Exit();
        }

        private void m_ProgressBar_MouseHover(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void DirectoryMatch_CheckedChanged(object sender, EventArgs e)
        {
  
        }

        private void m_DirectoryMatchCheckBx_CheckStateChanged(object sender, EventArgs e)
        {
            m_SelectClientInfoDirBtn.Enabled = !m_DirectoryMatchCheckBx.Checked;

            if (m_DirectoryMatchCheckBx.Checked)
            {
                if (m_TemplateFileTxtBox.Text == string.Empty)
                {
                    m_DirectoryMatchCheckBx.Checked = false;
                    MessageBox.Show("Please input the Template file path first.");
                }
                else
                {
                    m_ClientInformationDirectory.Text = Path.GetDirectoryName(m_TemplateFileTxtBox.Text);
                }
            }
            else
            {
                m_ClientInformationDirectory.Text = string.Empty;
            }

        }

        private void m_TemplateFileTxtBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
