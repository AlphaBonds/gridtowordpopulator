﻿namespace GridToWordPopulator
{
    partial class Converter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Converter));
            this.m_MainLayout = new System.Windows.Forms.TableLayoutPanel();
            this.m_DirectoryMatchCheckBx = new System.Windows.Forms.CheckBox();
            this.InstructionButton = new System.Windows.Forms.Button();
            this.m_InputPath = new System.Windows.Forms.GroupBox();
            this.m_TemplateFileTxtBox = new System.Windows.Forms.RichTextBox();
            this.m_SelectTemplateFileBtn = new System.Windows.Forms.Button();
            this.m_CreateBtn = new System.Windows.Forms.Button();
            this.m_OutputPath = new System.Windows.Forms.GroupBox();
            this.m_ClientInformationDirectory = new System.Windows.Forms.RichTextBox();
            this.m_SelectClientInfoDirBtn = new System.Windows.Forms.Button();
            this.m_FeedbackPanel = new System.Windows.Forms.Panel();
            this.m_StatusTable = new System.Windows.Forms.TableLayoutPanel();
            this.m_ProgressBar = new System.Windows.Forms.ProgressBar();
            this.m_FeedbackLbl = new System.Windows.Forms.Label();
            this.m_DateTimeLbl = new System.Windows.Forms.Label();
            this.m_ProcessKillBtns = new System.Windows.Forms.TableLayoutPanel();
            this.m_WordTerminateBtn = new System.Windows.Forms.Button();
            this.m_ExcelTerminate = new System.Windows.Forms.Button();
            this.m_DateTimeTimer = new System.Windows.Forms.Timer(this.components);
            this.m_MainLayout.SuspendLayout();
            this.m_InputPath.SuspendLayout();
            this.m_OutputPath.SuspendLayout();
            this.m_FeedbackPanel.SuspendLayout();
            this.m_StatusTable.SuspendLayout();
            this.m_ProcessKillBtns.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_MainLayout
            // 
            this.m_MainLayout.BackColor = System.Drawing.Color.LightGray;
            this.m_MainLayout.ColumnCount = 3;
            this.m_MainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.m_MainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.m_MainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.m_MainLayout.Controls.Add(this.m_DirectoryMatchCheckBx, 1, 3);
            this.m_MainLayout.Controls.Add(this.InstructionButton, 1, 1);
            this.m_MainLayout.Controls.Add(this.m_InputPath, 1, 2);
            this.m_MainLayout.Controls.Add(this.m_CreateBtn, 1, 6);
            this.m_MainLayout.Controls.Add(this.m_OutputPath, 1, 4);
            this.m_MainLayout.Controls.Add(this.m_FeedbackPanel, 1, 7);
            this.m_MainLayout.Controls.Add(this.m_DateTimeLbl, 0, 0);
            this.m_MainLayout.Controls.Add(this.m_ProcessKillBtns, 1, 8);
            this.m_MainLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_MainLayout.Location = new System.Drawing.Point(0, 0);
            this.m_MainLayout.Margin = new System.Windows.Forms.Padding(25);
            this.m_MainLayout.Name = "m_MainLayout";
            this.m_MainLayout.RowCount = 10;
            this.m_MainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.m_MainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.m_MainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.m_MainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.m_MainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.m_MainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.m_MainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.m_MainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.m_MainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.m_MainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.m_MainLayout.Size = new System.Drawing.Size(1001, 643);
            this.m_MainLayout.TabIndex = 0;
            this.m_MainLayout.Paint += new System.Windows.Forms.PaintEventHandler(this.m_MainLayout_Paint);
            // 
            // m_DirectoryMatchCheckBx
            // 
            this.m_DirectoryMatchCheckBx.AutoSize = true;
            this.m_DirectoryMatchCheckBx.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_DirectoryMatchCheckBx.Location = new System.Drawing.Point(43, 163);
            this.m_DirectoryMatchCheckBx.Name = "m_DirectoryMatchCheckBx";
            this.m_DirectoryMatchCheckBx.Size = new System.Drawing.Size(915, 24);
            this.m_DirectoryMatchCheckBx.TabIndex = 2;
            this.m_DirectoryMatchCheckBx.Text = "Set Client Information Directory to match template path.";
            this.m_DirectoryMatchCheckBx.UseVisualStyleBackColor = true;
            this.m_DirectoryMatchCheckBx.CheckedChanged += new System.EventHandler(this.DirectoryMatch_CheckedChanged);
            this.m_DirectoryMatchCheckBx.CheckStateChanged += new System.EventHandler(this.m_DirectoryMatchCheckBx_CheckStateChanged);
            // 
            // InstructionButton
            // 
            this.InstructionButton.BackColor = System.Drawing.Color.PaleTurquoise;
            this.InstructionButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.InstructionButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InstructionButton.Location = new System.Drawing.Point(43, 25);
            this.InstructionButton.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.InstructionButton.MaximumSize = new System.Drawing.Size(167, 34);
            this.InstructionButton.MinimumSize = new System.Drawing.Size(167, 34);
            this.InstructionButton.Name = "InstructionButton";
            this.InstructionButton.Size = new System.Drawing.Size(167, 34);
            this.InstructionButton.TabIndex = 7;
            this.InstructionButton.Text = "More Information";
            this.InstructionButton.UseVisualStyleBackColor = false;
            this.InstructionButton.Click += new System.EventHandler(this.InstructionButton_Click);
            // 
            // m_InputPath
            // 
            this.m_InputPath.Controls.Add(this.m_TemplateFileTxtBox);
            this.m_InputPath.Controls.Add(this.m_SelectTemplateFileBtn);
            this.m_InputPath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_InputPath.Location = new System.Drawing.Point(43, 83);
            this.m_InputPath.Name = "m_InputPath";
            this.m_InputPath.Size = new System.Drawing.Size(915, 74);
            this.m_InputPath.TabIndex = 1;
            this.m_InputPath.TabStop = false;
            this.m_InputPath.Text = "Template Path";
            // 
            // m_TemplateFileTxtBox
            // 
            this.m_TemplateFileTxtBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_TemplateFileTxtBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_TemplateFileTxtBox.Location = new System.Drawing.Point(3, 18);
            this.m_TemplateFileTxtBox.Name = "m_TemplateFileTxtBox";
            this.m_TemplateFileTxtBox.ReadOnly = true;
            this.m_TemplateFileTxtBox.Size = new System.Drawing.Size(765, 53);
            this.m_TemplateFileTxtBox.TabIndex = 1;
            this.m_TemplateFileTxtBox.Text = "";
            this.m_TemplateFileTxtBox.TextChanged += new System.EventHandler(this.m_TemplateFileTxtBox_TextChanged);
            // 
            // m_SelectTemplateFileBtn
            // 
            this.m_SelectTemplateFileBtn.BackColor = System.Drawing.SystemColors.ControlLight;
            this.m_SelectTemplateFileBtn.Dock = System.Windows.Forms.DockStyle.Right;
            this.m_SelectTemplateFileBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SelectTemplateFileBtn.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.m_SelectTemplateFileBtn.Location = new System.Drawing.Point(768, 18);
            this.m_SelectTemplateFileBtn.Margin = new System.Windows.Forms.Padding(50);
            this.m_SelectTemplateFileBtn.MaximumSize = new System.Drawing.Size(144, 55);
            this.m_SelectTemplateFileBtn.MinimumSize = new System.Drawing.Size(144, 55);
            this.m_SelectTemplateFileBtn.Name = "m_SelectTemplateFileBtn";
            this.m_SelectTemplateFileBtn.Padding = new System.Windows.Forms.Padding(5);
            this.m_SelectTemplateFileBtn.Size = new System.Drawing.Size(144, 55);
            this.m_SelectTemplateFileBtn.TabIndex = 0;
            this.m_SelectTemplateFileBtn.Text = "Select File";
            this.m_SelectTemplateFileBtn.UseVisualStyleBackColor = false;
            this.m_SelectTemplateFileBtn.Click += new System.EventHandler(this.m_SelectFileBtn_Click);
            // 
            // m_CreateBtn
            // 
            this.m_CreateBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.m_CreateBtn.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_CreateBtn.Location = new System.Drawing.Point(65, 300);
            this.m_CreateBtn.Margin = new System.Windows.Forms.Padding(25, 20, 25, 20);
            this.m_CreateBtn.Name = "m_CreateBtn";
            this.m_CreateBtn.Size = new System.Drawing.Size(871, 60);
            this.m_CreateBtn.TabIndex = 3;
            this.m_CreateBtn.Text = "Create Word Document";
            this.m_CreateBtn.UseVisualStyleBackColor = true;
            this.m_CreateBtn.Click += new System.EventHandler(this.m_CreateBtn_Click);
            // 
            // m_OutputPath
            // 
            this.m_OutputPath.Controls.Add(this.m_ClientInformationDirectory);
            this.m_OutputPath.Controls.Add(this.m_SelectClientInfoDirBtn);
            this.m_OutputPath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_OutputPath.Location = new System.Drawing.Point(43, 193);
            this.m_OutputPath.Name = "m_OutputPath";
            this.m_OutputPath.Size = new System.Drawing.Size(915, 74);
            this.m_OutputPath.TabIndex = 2;
            this.m_OutputPath.TabStop = false;
            this.m_OutputPath.Text = "Client Information Directory";
            // 
            // m_ClientInformationDirectory
            // 
            this.m_ClientInformationDirectory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_ClientInformationDirectory.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_ClientInformationDirectory.Location = new System.Drawing.Point(3, 18);
            this.m_ClientInformationDirectory.Name = "m_ClientInformationDirectory";
            this.m_ClientInformationDirectory.ReadOnly = true;
            this.m_ClientInformationDirectory.Size = new System.Drawing.Size(765, 53);
            this.m_ClientInformationDirectory.TabIndex = 1;
            this.m_ClientInformationDirectory.Text = "";
            // 
            // m_SelectClientInfoDirBtn
            // 
            this.m_SelectClientInfoDirBtn.BackColor = System.Drawing.SystemColors.ControlLight;
            this.m_SelectClientInfoDirBtn.Dock = System.Windows.Forms.DockStyle.Right;
            this.m_SelectClientInfoDirBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SelectClientInfoDirBtn.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.m_SelectClientInfoDirBtn.Location = new System.Drawing.Point(768, 18);
            this.m_SelectClientInfoDirBtn.MaximumSize = new System.Drawing.Size(144, 55);
            this.m_SelectClientInfoDirBtn.MinimumSize = new System.Drawing.Size(144, 55);
            this.m_SelectClientInfoDirBtn.Name = "m_SelectClientInfoDirBtn";
            this.m_SelectClientInfoDirBtn.Padding = new System.Windows.Forms.Padding(5);
            this.m_SelectClientInfoDirBtn.Size = new System.Drawing.Size(144, 55);
            this.m_SelectClientInfoDirBtn.TabIndex = 0;
            this.m_SelectClientInfoDirBtn.Text = "Select File";
            this.m_SelectClientInfoDirBtn.UseVisualStyleBackColor = false;
            this.m_SelectClientInfoDirBtn.Click += new System.EventHandler(this.SelectClientInfoDirBtn_Click);
            // 
            // m_FeedbackPanel
            // 
            this.m_FeedbackPanel.AutoSize = true;
            this.m_FeedbackPanel.Controls.Add(this.m_StatusTable);
            this.m_FeedbackPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_FeedbackPanel.Location = new System.Drawing.Point(43, 383);
            this.m_FeedbackPanel.Name = "m_FeedbackPanel";
            this.m_FeedbackPanel.Size = new System.Drawing.Size(915, 94);
            this.m_FeedbackPanel.TabIndex = 6;
            // 
            // m_StatusTable
            // 
            this.m_StatusTable.ColumnCount = 1;
            this.m_StatusTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.m_StatusTable.Controls.Add(this.m_ProgressBar, 0, 1);
            this.m_StatusTable.Controls.Add(this.m_FeedbackLbl, 0, 0);
            this.m_StatusTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_StatusTable.Location = new System.Drawing.Point(0, 0);
            this.m_StatusTable.Name = "m_StatusTable";
            this.m_StatusTable.RowCount = 2;
            this.m_StatusTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.m_StatusTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.m_StatusTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.m_StatusTable.Size = new System.Drawing.Size(915, 94);
            this.m_StatusTable.TabIndex = 6;
            // 
            // m_ProgressBar
            // 
            this.m_ProgressBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_ProgressBar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.m_ProgressBar.Location = new System.Drawing.Point(3, 50);
            this.m_ProgressBar.Name = "m_ProgressBar";
            this.m_ProgressBar.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.m_ProgressBar.Size = new System.Drawing.Size(909, 41);
            this.m_ProgressBar.TabIndex = 4;
            this.m_ProgressBar.Visible = false;
            this.m_ProgressBar.MouseHover += new System.EventHandler(this.m_ProgressBar_MouseHover);
            // 
            // m_FeedbackLbl
            // 
            this.m_FeedbackLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_FeedbackLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_FeedbackLbl.Location = new System.Drawing.Point(3, 0);
            this.m_FeedbackLbl.Name = "m_FeedbackLbl";
            this.m_FeedbackLbl.Size = new System.Drawing.Size(909, 47);
            this.m_FeedbackLbl.TabIndex = 5;
            this.m_FeedbackLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.m_FeedbackLbl.Click += new System.EventHandler(this.m_FeedbackLbl_Click);
            // 
            // m_DateTimeLbl
            // 
            this.m_DateTimeLbl.AutoSize = true;
            this.m_MainLayout.SetColumnSpan(this.m_DateTimeLbl, 2);
            this.m_DateTimeLbl.Location = new System.Drawing.Point(3, 5);
            this.m_DateTimeLbl.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.m_DateTimeLbl.Name = "m_DateTimeLbl";
            this.m_DateTimeLbl.Size = new System.Drawing.Size(0, 15);
            this.m_DateTimeLbl.TabIndex = 8;
            this.m_DateTimeLbl.Click += new System.EventHandler(this.label1_Click);
            // 
            // m_ProcessKillBtns
            // 
            this.m_ProcessKillBtns.ColumnCount = 2;
            this.m_ProcessKillBtns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.m_ProcessKillBtns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.m_ProcessKillBtns.Controls.Add(this.m_WordTerminateBtn, 0, 1);
            this.m_ProcessKillBtns.Controls.Add(this.m_ExcelTerminate, 1, 1);
            this.m_ProcessKillBtns.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_ProcessKillBtns.Location = new System.Drawing.Point(43, 483);
            this.m_ProcessKillBtns.Name = "m_ProcessKillBtns";
            this.m_ProcessKillBtns.RowCount = 2;
            this.m_ProcessKillBtns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.m_ProcessKillBtns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.m_ProcessKillBtns.Size = new System.Drawing.Size(915, 94);
            this.m_ProcessKillBtns.TabIndex = 9;
            // 
            // m_WordTerminateBtn
            // 
            this.m_WordTerminateBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_WordTerminateBtn.Location = new System.Drawing.Point(50, 50);
            this.m_WordTerminateBtn.Margin = new System.Windows.Forms.Padding(50, 3, 50, 3);
            this.m_WordTerminateBtn.Name = "m_WordTerminateBtn";
            this.m_WordTerminateBtn.Size = new System.Drawing.Size(357, 41);
            this.m_WordTerminateBtn.TabIndex = 1;
            this.m_WordTerminateBtn.Text = "Terminate all Word processes.";
            this.m_WordTerminateBtn.UseVisualStyleBackColor = true;
            this.m_WordTerminateBtn.Click += new System.EventHandler(this.m_WordTerminate_Click);
            // 
            // m_ExcelTerminate
            // 
            this.m_ExcelTerminate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_ExcelTerminate.Location = new System.Drawing.Point(507, 50);
            this.m_ExcelTerminate.Margin = new System.Windows.Forms.Padding(50, 3, 50, 3);
            this.m_ExcelTerminate.Name = "m_ExcelTerminate";
            this.m_ExcelTerminate.Size = new System.Drawing.Size(358, 41);
            this.m_ExcelTerminate.TabIndex = 0;
            this.m_ExcelTerminate.Text = "Terminate all Excel processes";
            this.m_ExcelTerminate.UseVisualStyleBackColor = true;
            this.m_ExcelTerminate.Click += new System.EventHandler(this.m_ExcelTerminate_Click);
            // 
            // m_DateTimeTimer
            // 
            this.m_DateTimeTimer.Enabled = true;
            this.m_DateTimeTimer.Tick += new System.EventHandler(this.m_DateTimeTimer_Tick);
            // 
            // Converter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlText;
            this.ClientSize = new System.Drawing.Size(1001, 643);
            this.Controls.Add(this.m_MainLayout);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Converter";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Grid To Word Populator";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.m_MainLayout.ResumeLayout(false);
            this.m_MainLayout.PerformLayout();
            this.m_InputPath.ResumeLayout(false);
            this.m_OutputPath.ResumeLayout(false);
            this.m_FeedbackPanel.ResumeLayout(false);
            this.m_StatusTable.ResumeLayout(false);
            this.m_ProcessKillBtns.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel m_MainLayout;
        private System.Windows.Forms.GroupBox m_InputPath;
        private System.Windows.Forms.Button m_SelectTemplateFileBtn;
        private System.Windows.Forms.RichTextBox m_TemplateFileTxtBox;
        private System.Windows.Forms.GroupBox m_OutputPath;
        private System.Windows.Forms.RichTextBox m_ClientInformationDirectory;
        private System.Windows.Forms.Button m_SelectClientInfoDirBtn;
        private System.Windows.Forms.Button m_CreateBtn;
        private System.Windows.Forms.Panel m_FeedbackPanel;
        private System.Windows.Forms.Label m_FeedbackLbl;
        private System.Windows.Forms.Button InstructionButton;
        private System.Windows.Forms.ProgressBar m_ProgressBar;
        private System.Windows.Forms.TableLayoutPanel m_StatusTable;
        private System.Windows.Forms.Label m_DateTimeLbl;
        private System.Windows.Forms.Timer m_DateTimeTimer;
        private System.Windows.Forms.TableLayoutPanel m_ProcessKillBtns;
        private System.Windows.Forms.Button m_WordTerminateBtn;
        private System.Windows.Forms.Button m_ExcelTerminate;
        private System.Windows.Forms.CheckBox m_DirectoryMatchCheckBx;
    }
}

