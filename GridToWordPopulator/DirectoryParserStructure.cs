﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GridToWordPopulator
{
    public class DirectoryParserStructure
    {
        private string m_FilePath;
        private string m_ParserInfo;
        private Dictionary<string, DirectoryParserStructure> m_Children; 

        public DirectoryParserStructure(string filePath, string parserInfo)
        {
            m_FilePath = filePath;
            m_ParserInfo = parserInfo;
            m_Children = new Dictionary<string, DirectoryParserStructure>();
        }

        public string FilePath
        {
            get
            {
                return m_FilePath;
            }
        }

        public void AppendChild(DirectoryParserStructure parserStructure)
        {
            if (m_Children.ContainsKey(parserStructure.FilePath))
            {
                throw new AccessViolationException("Child already exists");
            }
            else 
            {
                m_Children[parserStructure.FilePath] = parserStructure;
            }
        }

        public void RemoveChild(string filePath)
        {
            if (m_Children.ContainsKey(filePath))
            {
                m_Children.Remove(filePath);
            }
        }

        public void RemoveChild(DirectoryParserStructure parserStructure)
        {
            if (m_Children.ContainsKey(parserStructure.FilePath))
            {
                m_Children.Remove(parserStructure.FilePath);
            }
        }
    }
}
