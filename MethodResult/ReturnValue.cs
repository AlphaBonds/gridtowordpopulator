﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MethodResult
{
    public class ReturnValue
    {
        private bool m_Success;
        private bool m_Warning; 
        private string m_StatusMsg;
        public object ErrorType;

        public ReturnValue()
        {
            m_Success = true;
            m_Warning = false; 
            m_StatusMsg = string.Empty;
        }

        public void SetNoIssues()
        {
            Success = true;
            StatusMessage = string.Empty; 
        }

        public bool Success
        {
            get
            {
                return m_Success;
            }
            set
            {
                m_Success = value;

                if (value != true && StatusMessage == string.Empty)
                {
                    StatusMessage = Strings.Error;
                }
            }
        }

        public string StatusMessage
        {
            get
            {
                return m_StatusMsg;
            }
            set
            {
                m_StatusMsg = value; 
            }
        }

        public bool Warnings
        {
            get
            {
                return m_Warning;
            }
            set
            {
                m_Warning = value;


                if (value != true && StatusMessage == string.Empty)
                {
                    StatusMessage = Strings.Warning;
                }
            }
        }
    }
}
