﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CSVManager
{
    public class CSVWorker
    {
        private string m_FileName = string.Empty;

        public CSVWorker(string fileName)
        {
            m_FileName = fileName;
        }

        public void AddCells(int row, int column, string newValue)
        {
            var encoding = Encoding.GetEncoding("iso-8859-1");
            var csvLines = File.ReadAllLines(m_FileName, encoding);

            if (row < csvLines.Length)
            {         
                ReplaceCells(row, column, newValue);         
            }
            else
            {
                using (FileStream stream = new FileStream(m_FileName, FileMode.Create))
                {
                    using (StreamWriter writer = new StreamWriter(stream, encoding))
                    {
                        foreach (var line in csvLines)
                        {
                            writer.WriteLine(line);
                        }

                        int blankLines = row - csvLines.Length - 1;
                       
                        for (int i =  0; i < blankLines; i++)
                        {
                            writer.WriteLine("");
                        }

                        string blankCols = string.Empty;

                        for (int i = 0; i < column-1; i++)
                        {
                            blankCols += ',';
                        }

                        writer.WriteLine(blankCols + newValue);                       
                    }
                }
            }
        }

        public void ReplaceCells(int row, int column, string newValue)
        {
            var encoding = Encoding.GetEncoding("iso-8859-1");
            var csvLines = File.ReadAllLines(m_FileName, encoding);

            for (int i = 0; i < csvLines.Length; i++)
            {
                //var values = csvLines[i].Split(',');
                List <string> values = csvLines[i].Split(',').ToList();

                if (i == row)
                {
                    if (column < values.Count)
                    {
                        values[column] = newValue;
                    }
                    else 
                    {
                        while (values.Count < column - 1)
                        {
                            values.Append(",");
                        }

                        values.Append(newValue);
                    }

                    using (FileStream stream = new FileStream(m_FileName, FileMode.Create))
                    {
                        using (StreamWriter writer = new StreamWriter(stream, encoding))
                        {
                            for (int currentLine = 0; currentLine < csvLines.Length; ++currentLine)
                            {
                                if (currentLine == i)
                                {
                                    writer.WriteLine(string.Join(",", values));
                                }
                                else
                                {
                                    writer.WriteLine(csvLines[currentLine]);
                                }
                            }

                            writer.Close();
                        }

                        stream.Close();
                        break;
                    }
                }
            }
        }
    }
}
